﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinSwappingController : MonoBehaviour
{
    public Transform playerSkinsHolder;
    public Transform hookSkinsHolder;

    public int SelectedPlayerSkin { get => selectedPlayerSkin; }
    public int SelectedHookSkin { get => selectedHookSkin; }

    [SerializeField] private List<PlayerSkin> playerSkins;
    private int selectedPlayerSkin = -1;

    private List<GameObject> hookSkins;
    private int selectedHookSkin = -1;

    void Awake()
    {
        hookSkins = new List<GameObject>();
        foreach (Transform hookSkinObject in hookSkinsHolder)
        {
            hookSkins.Add(hookSkinObject.gameObject);
        }

        EventBroker.OnPlayerSkinChange += (skinNumber) =>
        {
            if (selectedPlayerSkin == skinNumber)
                return;

            selectedPlayerSkin = skinNumber;
            SetPlayerSkins(playerSkins, selectedPlayerSkin);
        };

        EventBroker.OnHookSkinChange += (skinNumber) =>
        {
            if (selectedHookSkin == skinNumber)
                return;

            selectedHookSkin = skinNumber;
            SetHookSkins(hookSkins, selectedHookSkin);
        };
    }

    void SetHookSkins(List<GameObject> skins, int skinNumber)
    {
        int i = 0;
        foreach(var skin in skins)
        {
            if (i == skinNumber)
            {
                skin.SetActive(true);
            }
            else
            {
                skin.SetActive(false);
            }
            i++;
        }
    }

    void SetPlayerSkins(List<PlayerSkin> skins, int skinNumber)
    {
        int i = 0;
        foreach (var skin in skins)
        {
            if (i == skinNumber)
            {
                skin.SetSkinVisiblility(true);
            }
            else
            {
                skin.SetSkinVisiblility(false);
            }
            i++;
        }
    }

    public void DisablePlayerSkinSprite()
    {
        playerSkins[selectedPlayerSkin].SetSkinVisiblility(false);
    }


    public void EnablePlayerSkinSprite()
    {
        playerSkins[selectedPlayerSkin].SetSkinVisiblility(true);
    }

    public void PlayDeathParticles()
    {
        playerSkins[selectedPlayerSkin].PlayDeathParticles();

    }

    public void StopDeathParticles()
    {
        playerSkins[selectedPlayerSkin].StopDeathParticles();
    }
}
