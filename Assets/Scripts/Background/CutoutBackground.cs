using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutoutBackground : ScrollingBackground
{
    protected override void Cutout()
    {
        if (transform.parent.parent.position.y > 2013)
        {
            transform.position = new Vector3(transform.parent.parent.position.x, 2013, 100);
            //Debug.Log("ABOVE " + transform.localPosition);
        }
        else
        {
            if (transform.localPosition.y != 0)
            {
                //Debug.Log("O K A Y");
                transform.localPosition = new Vector3(0, 0, 0);
            }
        }
    }
}
