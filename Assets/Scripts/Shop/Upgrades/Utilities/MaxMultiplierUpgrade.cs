﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MaxMultiplier", menuName = "Upgrade/Max Multiplier")]
public class MaxMultiplierUpgrade : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        player.currencyData.currencyController.currentMaxMultiplier = player.currencyData.currencyController.baseMaxMultiplier + (CurrentLevel * (int)IncreaseValuePerLevel);
    }
}
