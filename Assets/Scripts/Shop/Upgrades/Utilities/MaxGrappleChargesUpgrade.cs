﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MaxGrappleCharges", menuName = "Upgrade/Max Grapple Charges")]
public class MaxGrappleChargesUpgrade : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        player.grapplingGun.currentMaxGrappleCharges = player.grapplingGun.baseMaxGrappleCharges + (CurrentLevel * (int)IncreaseValuePerLevel);
    }
}
