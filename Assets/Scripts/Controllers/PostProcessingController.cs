﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class PostProcessingController : MonoBehaviour
{
    private Volume volume;
    private Bloom bloom;
    private ChromaticAberration chromaticAberration;
    private LensDistortion lensDistortion;
    private ColorAdjustments colorAdjustments;

    public float bigChromaticValue;
    public float smallChromaticValue;

    private float chromAberrTargetValue = 0.1f;
    private float chromAberrVelocitySize = 0f;

    private float lensDistorTargetValue = 0.1f;
    private float lensDistorVelocitySize = 0f;

    private float saturationTargetValue = 0.1f;
    private float saturationVelocitySize = 0f;

    void Awake()
    {
        volume = GetComponent<Volume>();
        volume.profile.TryGet(out bloom);
        volume.profile.TryGet(out chromaticAberration);
        volume.profile.TryGet(out lensDistortion);
        volume.profile.TryGet(out colorAdjustments);
    }

    private void Update()
    {
        if (chromaticAberration.intensity.value >= chromAberrTargetValue + 0.01 || chromaticAberration.intensity.value <= chromAberrTargetValue - 0.01)
        {
            chromaticAberration.intensity.value = Mathf.SmoothDamp(chromaticAberration.intensity.value, chromAberrTargetValue, ref chromAberrVelocitySize, 0.3f, Mathf.Infinity, Time.unscaledDeltaTime);
        }

        if (lensDistortion.intensity.value >= lensDistorTargetValue + 0.01 || lensDistortion.intensity.value <= lensDistorTargetValue - 0.01)
        {
            lensDistortion.intensity.value = Mathf.SmoothDamp(lensDistortion.intensity.value, lensDistorTargetValue, ref lensDistorVelocitySize, 0.3f, Mathf.Infinity, Time.unscaledDeltaTime);
        }

        if (colorAdjustments.saturation.value >= saturationTargetValue + 0.1 || colorAdjustments.saturation.value <= saturationTargetValue - 0.1)
        {
            colorAdjustments.saturation.value = Mathf.SmoothDamp(colorAdjustments.saturation.value, saturationTargetValue, ref saturationVelocitySize, 0.3f, Mathf.Infinity, Time.unscaledDeltaTime);
        }
    }

    public void SetChromaticAberration(bool isBigger)
    {
        if (isBigger)
        {
            chromAberrTargetValue = bigChromaticValue;
        }
        else
        {
            chromAberrTargetValue = smallChromaticValue;
        }
    }

    public void SetLensDistortion(float value)
    {
        lensDistorTargetValue = value;
    }

    public void SetSaturation(float value)
    {
        saturationTargetValue = value;
    }
}
