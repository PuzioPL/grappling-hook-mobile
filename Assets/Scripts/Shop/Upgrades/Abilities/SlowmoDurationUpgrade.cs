﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SlowmoDuration", menuName = "Upgrade/Slowmo Duration")]
public class SlowmoDurationUpgrade : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        player.grapplingGun.slowmoController.currentMaxSlowmoDuration = player.grapplingGun.slowmoController.baseMaxSlowmoDuration + (CurrentLevel * IncreaseValuePerLevel);
    }
}
