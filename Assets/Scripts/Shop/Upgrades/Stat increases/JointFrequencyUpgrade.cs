﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "JointFrequency", menuName = "Upgrade/Joint Frequency")]
public class JointFrequencyUpgrade : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        player.grapplingGun.currentJointFrequency = player.grapplingGun.baseJointFrequency + (CurrentLevel * IncreaseValuePerLevel);
    }
}
