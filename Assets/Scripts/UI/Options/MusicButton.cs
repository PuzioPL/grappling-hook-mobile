using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicButton : MuteAudioButtonBase
{
    public override void SendEvent()
    {
        EventBroker.CallOnSetMusic(isAudioOn);
    }
}
