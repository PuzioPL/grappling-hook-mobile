using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkin : MonoBehaviour
{
    [SerializeField] private GameObject spriteObject;
    [SerializeField] private ParticleSystem deathParticles;

    public void SetSkinVisiblility(bool isVisible)
    {
        spriteObject.SetActive(isVisible);
        StopDeathParticles();
    }

    public void PlayDeathParticles()
    {
        deathParticles.Play();
    }

    public void StopDeathParticles()
    {
        deathParticles.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
    }
}