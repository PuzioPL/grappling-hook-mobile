﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerBaseState
{
    public abstract void EnterState(PlayerController player);
    public abstract void Update(PlayerController player);
    public abstract void OnCollisionStay(PlayerController player);
    public abstract void OnCollisionExit(PlayerController player);
}

