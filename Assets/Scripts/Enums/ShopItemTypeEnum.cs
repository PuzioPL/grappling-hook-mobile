﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShopItemTypeEnum
{
    StatIncrease,
    Ability,
    Utility,
    ZonesUtility,
    SkinPlayer,
    SkinHook
}
