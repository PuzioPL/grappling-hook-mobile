﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

public class SaveSystem : MonoBehaviour
{
    public ShopController shopController;
    public ItemsPickupController itemsPickupController; 
    public CurrencyData_SO currencyData;
    public OptionsUIController optionsUIController;
    public MusicButton muteMusicButton;
    public SoundEffectsButton muteSoundEffectsButton;
    public GatesController gateController;

    private void OnEnable()
    {
        var stateData = LoadState();

        shopController.AddAbilitiesAndSkinsToList();

        if (stateData != null)
        {
            shopController.SetItems(stateData.upgradesData);
            shopController.SetItems(stateData.skinsData);

            currencyData.CurrentCurrency = stateData.currency;

            itemsPickupController.SetItemPicked(ItemSide.Right, stateData.isRightItemPicked, false);
            itemsPickupController.SetItemPicked(ItemSide.Left, stateData.isLeftItemPicked, false);
            itemsPickupController.SetItemPicked(ItemSide.Top, stateData.isTopItemPicked, false);

            shopController.SetSecretItems(stateData.isTopItemPicked);
            optionsUIController.isBetterGraphics = stateData.isBetterGraphics;

            muteMusicButton.SetAudio(stateData.isMusicOn);
            muteSoundEffectsButton.SetAudio(stateData.isSoundEffectsOn);

            for (var i = 0; i < gateController.gatesHolders.Count; i++)
            {
                if (stateData.gatesData[i])
                {
                    gateController.gatesHolders[i].gate.OpenSesame(false);
                }
            }
        }
        else
        {
            shopController.SetItems(null);
            //currencyData.CurrentCurrency = 0; COMMENTED FOR TESTING PURPOSES - UNCOMMENT LATER
            //currencyData.CurrentCurrency = 99999;
            currencyData.CurrentCurrency = 0;

            itemsPickupController.SetItemPicked(ItemSide.Right, false, false);
            itemsPickupController.SetItemPicked(ItemSide.Left, false, false);
            itemsPickupController.SetItemPicked(ItemSide.Top, false, false);

            shopController.SetSecretItems(false);
            optionsUIController.isBetterGraphics = true;

            muteMusicButton.SetAudio(true);
            muteSoundEffectsButton.SetAudio(true);
        }
    }

    private void Start()
    {
        // wysyłam jeszcze raz na starcie event, ponieważ z jakiegoś powodu ustawianie parametrów mixera w Awake'u 
        //    (czyli wtedy kiedy zczytuje dane zapisu) nie działa, i trzeba to robić w Starcie
        // FUN FACT: próbowałem to robić w Starcie każdego z mute przycisku, ale Z JAKIEGOŚ POWODU metoda Start przycisku SoundEffect
        //    wykonywała się dopiero po włączeniu OptionsUI z menu. 
        //    Ogólnie chyba Start() nie wywoła się, jeżeli obiekt jest wyłączony i wykona się dopiero, kiedy się go włączy. OH WELL ¯\_(ツ)_/¯
        muteMusicButton.SendEvent();
        muteSoundEffectsButton.SendEvent();
    }

    private void OnApplicationQuit()
    {
        SaveState();
    }
    private void OnApplicationPause(bool isPaused)
    {
        SaveState();
    }

    public void SaveState()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/data.yeet";
        FileStream stream = new FileStream(path, FileMode.Create);

        SaveData data = new SaveData(
            currencyData.CurrentCurrency, 
            shopController.shopItems, 
            itemsPickupController.rightItem.isPickedUp, 
            itemsPickupController.leftItem.isPickedUp, 
            itemsPickupController.topItem.isPickedUp,
            optionsUIController.isBetterGraphics,
            muteMusicButton.isAudioOn,
            muteSoundEffectsButton.isAudioOn,
            gateController.gatesHolders);

        formatter.Serialize(stream, data);
        stream.Close();
    }

    public SaveData LoadState()
    {
        string path = Application.persistentDataPath + "/data.yeet";

        if (File.Exists(path))
        {
            var formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            SaveData data = formatter.Deserialize(stream) as SaveData;
            stream.Close();

            return data;
        }
        else
        {
            Debug.LogWarning("Save file not found in " + path);
            return null;
        }
    }
}
