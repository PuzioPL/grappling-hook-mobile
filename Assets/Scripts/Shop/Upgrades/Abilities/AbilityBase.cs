﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbilityBase : UpgradeBase
{
    [Header("Ability specific properties")]
    public float BaseCooldown;
    public float CurrentCooldown;
    public float NextTimeToUse = 0;

    public bool IsActive = false;
    public bool IsAlreadyAdded = false;

    public AbilityBase()
    {
        EventBroker.OnPlayerDeath += () => NextTimeToUse = 0;
    }

    public void Use(PlayerController player)
    {
        if (NextTimeToUse <= Time.time)
        {
            Perform(player);

            NextTimeToUse = Time.time + CurrentCooldown;
        }
    }

    public abstract void Perform(PlayerController player);
}
