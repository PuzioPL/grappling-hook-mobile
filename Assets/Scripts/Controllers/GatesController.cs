﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GatesController : MonoBehaviour
{
    public SkinSwappingController skinSwappingController;
    public ItemsPickupController itemsPickupController;

    public List<GateHolder> gatesHolders;

    void Start()
    {
        foreach(var holder in gatesHolders)
        {
            holder.Assign(this);
        }
    }
}

[System.Serializable]
public class GateHolder
{
    public Gate gate;
    public Condition condition;

    public void Assign(GatesController controller)
    {
        gate.gateCondition = condition;
        condition.Assign(controller);
    }
}