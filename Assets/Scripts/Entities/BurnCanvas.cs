using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BurnCanvas : MonoBehaviour
{
    public Transform playerTransform;
    public Slider burnSlider;
    public Image fill;

    public float baseMaxBurnValue;
    public float currentMaxBurnValue;
    public float increasedMaxBurnValue;
    public float startingColorHue;

    private float burnValue;
    private bool isBurning;
    private float burnPercent;

    private void Awake()
    {
        burnValue = 0;
        isBurning = false;

        EventBroker.OnBurnMeterSet += (isStart) => isBurning = isStart;
        //EventBroker.CallOnRedEdgeUpgrade += () => currentMaxBurnValue
    }

    void Update()
    {
        transform.position = (Vector2)playerTransform.position + new Vector2(0, 4);

        if (isBurning)
        {
            burnValue += Time.deltaTime;
        }
        else
        {
            burnValue -= Time.deltaTime/ 2;
        }

        burnPercent = burnValue / currentMaxBurnValue;

        burnSlider.value = burnPercent;

        if (burnValue >= currentMaxBurnValue)
        {
            EventBroker.CallOnPlayerDeath();
            burnValue = 0;
            gameObject.SetActive(false);
        }
        
        if (burnValue <= 0)
        {
            burnValue = 0;
            gameObject.SetActive(false);
        }

        SetMeterColor();
    }

    void SetMeterColor()
    {
        var inverselyProportionate = Mathf.InverseLerp(1, 0, burnPercent);

        var newHue = Mathf.Lerp(0, startingColorHue, inverselyProportionate);

        fill.color = Color.HSVToRGB(newHue, 1, 1);
    }

    public void StartBurn()
    {
        isBurning = true;
    }

    public void StopBurn()
    {
        isBurning = false;
    }

    public void DecreaseMaxBurnValue()
    {
        currentMaxBurnValue = baseMaxBurnValue;
    }

    public void IncreaseMaxBurnValue()
    {
        currentMaxBurnValue = increasedMaxBurnValue;
    }
}
