﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EdgeBase : InteractableObjectBase
{
    private void Awake()
    {
        CurrencyReward = 50;
    }

    public override void OnPlayerCollision(PlayerController player)
    {
    }

    public override void OnHookCollision(HookScript hook, float distance, Vector2 hookPosition)
    {
        EventBroker.CallOnAddCurrency(CurrencyReward, distance, hookPosition);
    }
}
