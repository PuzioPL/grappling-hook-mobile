using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnTrigger : MonoBehaviour, ITriggerBehaviour
{
    void ITriggerBehaviour.Act(bool isEnter)
    {
        EventBroker.CallOnPlayerInBurnPresence(isEnter);
    }
}
