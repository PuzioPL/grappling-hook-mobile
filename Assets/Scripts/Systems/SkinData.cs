﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SkinData : SaveableDataBase
{
    public bool isUnlocked;
    public bool isSelected;

    public SkinData(int skinId, bool unlocked, bool selected)
    {
        id = skinId;
        isUnlocked = unlocked;
        isSelected = selected;
    }
}
