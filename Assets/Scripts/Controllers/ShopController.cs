﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class ShopController : MonoBehaviour
{
    public PlayerController player;
    public List<ShopItemBase> shopItems;

    public AbilityBase ability1;
    public AbilityBase ability2;

    public List<SkinBase> playerSkins;
    public List<SkinBase> hookSkins;

    public SkinBase defaultPlayerSkin;
    public SkinBase defaultHookSkin;

    public List<ShopItemBase> hiddenItems;
    public List<ShopItemBase> placeholderItems;

    public event Action OnRevealItems;

    private void Start()
    {
        EventBroker.OnItemPickup += (itemSide, showPopup) =>
        {
            if (itemSide == ItemSide.Top)
            {
                SetSecretItems(true);
                OnRevealItems.Invoke();
            }
        };
    }

    // Próbowałem dodać je w Awake'u, ale SaveSystem odczytywał z zapisu dane, zanim wykonał się Awake tutaj, przez co Abilities były pomijane.
    // zrobiłem więc metodę, którą wywołuje przed wczytaniem danych z zapisu
    public void AddAbilitiesAndSkinsToList()
    {
        shopItems.Insert(0, ability1);
        shopItems.Insert(1, ability2);
        shopItems.AddRange(playerSkins);
        shopItems.AddRange(hookSkins);
    }

    public void SetItems(SaveableDataBase[] shopItemsData = null)
    {
        if (shopItemsData != null)
        {
            foreach (var shopItem in shopItems)
            {
                var chosenItem = shopItemsData.Where(p => p.id == shopItem.Id).FirstOrDefault();

                if (chosenItem != null)
                {
                    shopItem.ReadSaveData(chosenItem);
                }
                else
                {
                    continue;
                }

                shopItem.ApplyItem(player);
            }
        }
        else
        {
            foreach (var shopItem in shopItems)
            {
                shopItem.ReadSaveData(null);
                shopItem.ApplyItem(player);
            }
            defaultPlayerSkin.Buy(player);
            defaultHookSkin.Buy(player);
        }
    }

    public void SetSecretItems(bool isReveal)
    {
        if (isReveal)
        {
            // show actual, hide placeholders
            foreach (var hiddenItem in hiddenItems)
            {
                hiddenItem.IsHidden = false;
            }

            foreach (var placeholder in placeholderItems)
            {
                placeholder.IsHidden = true;
            }
        }
        else
        {
            // show placeholder, hide actual
            foreach (var hiddenItem in hiddenItems)
            {
                hiddenItem.IsHidden = true;
            }

            foreach (var placeholder in placeholderItems)
            {
                placeholder.IsHidden = false;
            }
        }
    }
}
