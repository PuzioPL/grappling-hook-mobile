using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionArrowController : MonoBehaviour
{
    public DirectionArrow directionArrow;
    public Transform enemy;
    public Vector2 leftItemPosition;

    public Color enemyArrowColor;
    public Color itemArrowColor;

    private ItemsPickupController itemsPickupController;

    void Awake()
    {
        itemsPickupController = GetComponent<ItemsPickupController>();

        EventBroker.OnPlayerInMazePresence += (isEnter) =>
        {
            InitializeArrow(isEnter, enemy, enemyArrowColor);
        };

        EventBroker.OnPlayerInLeftSecretPresence += (isEnter) =>
        {
            if (!itemsPickupController.leftItem.isPickedUp)
            {
                InitializeArrow(isEnter, itemsPickupController.leftItemInstance.gameObject.transform, itemArrowColor);
            }
        };

        EventBroker.OnItemPickup += (itemSide, isShowPopup) =>
        {
            if (itemSide == ItemSide.Left)
            {
                directionArrow.gameObject.SetActive(false);
            }
        };
    }

    void InitializeArrow(bool isEnter, Transform target, Color color)
    {
        directionArrow.gameObject.SetActive(isEnter);

        if (isEnter)
        {
            directionArrow.SetupArrow(target, color);
        }
    }
}
