﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MultiplierDuration", menuName = "Upgrade/Multiplier Duration")]
public class MultiplierDurationUpgrade : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        player.currencyData.currencyController.currentMaxMultiplierDuration = player.currencyData.currencyController.baseMaxMultiplierDuration + (CurrentLevel * (int)IncreaseValuePerLevel);
    }
}
