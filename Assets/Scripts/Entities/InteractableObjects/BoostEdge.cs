﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostEdge : InteractableObjectBase
{

    private float addedForce = 75;

    void Awake()
    {
        IsGrappable = true;
    }

    public override void OnPlayerCollision(PlayerController player)
    {
        player.rig2d.velocity += player.rig2d.velocity.normalized * addedForce;
        player.grapplingGun.SeverGrapple();
        player.ApplyTorque(transform.position, player.rig2d.velocity.magnitude / 2);
        AudioManager.instance.PlayFromManager(4);
    }
    public override void OnHookCollision(HookScript hook, float distance, Vector2 hookPosition)
    {

    }
}
