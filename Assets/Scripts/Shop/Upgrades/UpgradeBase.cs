﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UpgradeBase : ShopItemBase
{
    [Header("Upgrade specific fields")]
    public int MaxLevel;
    public int CurrentLevel;
    public  List<int> PricesForLevel;
    public  int PriceForNextLevel { get { return PricesForLevel[CurrentLevel + 1]; } }
    public float IncreaseValuePerLevel;

    public override string GetButtonMainText()
    {
        if (!IsMaxed())
        {
            return $"{Text}\n{CurrentLevel}/{MaxLevel}";
        }
        else
        {
            if (!IsPlaceholder)
            {
                return $"{Text}\nMAX";
            }
            else
            {
                return $"{Text}";
            }
        }
    }

    public override string GetButtonPriceText()
    {
        if (!IsMaxed())
        {
            return $"{PriceForNextLevel}$";
        }
        else
        {
            return $"";
        }
    }

    public override int GetPrice()
    {
        return PriceForNextLevel;
    }

    public override bool IsAffordable(int currentCurrency)
    {
        var tempCurrency = currentCurrency - PriceForNextLevel;

        if (tempCurrency >= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public override bool IsMaxed()
    {
        return CurrentLevel >= MaxLevel;
    }

    public override void Buy(PlayerController player)
    {
        if (CurrentLevel < MaxLevel)
        {
            CurrentLevel++;
            ApplyItem(player);
        }
    }

    public override void OnSelect(PlayerController player) { }

    public override void ReadSaveData(SaveableDataBase data = null)
    {
        if (data != null)
        {
            var upgradeData = data as UpgradeData;
            CurrentLevel = upgradeData.currentLevel;
        }
        else
        {
            CurrentLevel = 0;
        }
    }
}
