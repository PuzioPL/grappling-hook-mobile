﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopState : GameStateBase
{
    public ShopState(GameStateController controller) : base(controller)
    {
    }

    public override void OnEnterState()
    {
        stateController.uiController.SwitchUI(GameStatesEnum.Shop);
        stateController.uiController.SetJoystick(false);
        stateController.uiController.shopUIController.OpacityOnUpgrades();
    }

    public override void OnExitState()
    {
        EventBroker.CallOnRespawn();
    }

    public override void RetryButtonEvent()
    {
        stateController.TransitionToState(stateController.PlayState);
    }

    public override void BackButtonEvent()
    {
        stateController.TransitionToState(stateController.MainMenuState);
    }
}
