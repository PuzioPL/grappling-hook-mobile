﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeTrigger : MonoBehaviour, ITriggerBehaviour
{
    void ITriggerBehaviour.Act(bool isEnter)
    {
        EventBroker.CallOnPlayerInMazePresence(isEnter);
    }
}