﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FlipVelocity", menuName = "Upgrade/Ability/Flip Velocity")]
public class FlipVelocityAbility : AbilityBase
{
    public override void Perform(PlayerController player)
    {
        var velocity = player.rig2d.velocity;
        player.rig2d.velocity = new Vector2(velocity.x, -velocity.y);
        player.grapplingGun.ApplyGrappleCharges(1);
        AudioManager.instance.PlayFromManager(11);
    }

    public override void ApplyItem(PlayerController player)
    {
        if (CurrentLevel > 0)
        {
            IsActive = true;
        }
        else
        {
            IsActive = false;
            IsAlreadyAdded = false;
            NextTimeToUse = 0;
        }

        CurrentCooldown = BaseCooldown - (CurrentLevel * IncreaseValuePerLevel);
        NextTimeToUse = 0;
    }
}
