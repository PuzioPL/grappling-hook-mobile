using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class AdsWindow : MonoBehaviour
{
    public TextMeshProUGUI amountText;
    public Button watchAdButton;
    public Button declineButton;

    public Action<bool> isChoseWatchAd;

    public void Initialize(Action showRewardedVideoAction)
    {
        watchAdButton.onClick.AddListener(() =>
        {
            showRewardedVideoAction.Invoke();
            isChoseWatchAd.Invoke(true);
            AudioManager.instance.TurnMasterVolume(false);
        });

        declineButton.onClick.AddListener(() =>
        {
            isChoseWatchAd.Invoke(false);
            gameObject.SetActive(false);
            AudioManager.instance.PlayFromManager(1);
        });

        gameObject.SetActive(false);
    }

    public void SetRewardValue(int amountToGain)
    {
        amountText.text = $"(+{amountToGain}$)";
    }
}
