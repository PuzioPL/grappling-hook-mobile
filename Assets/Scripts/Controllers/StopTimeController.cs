﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopTimeController : MonoBehaviour
{
    public bool isActive;

    public PostProcessingController postProcessingController;
    public TimeManager timeManager;
    public LineRenderer lr;

    public bool isStopTime;

    public void StartStopTime()
    {
        if (!isStopTime)
        {
            lr.enabled = true;
            isStopTime = true;
            timeManager.DoAlterTime(0);
            postProcessingController.SetChromaticAberration(true);
            postProcessingController.SetLensDistortion(0.3f);
            postProcessingController.SetSaturation(-50);
            AudioManager.instance.PlayFromManager(8);
            AudioManager.instance.StartLowpassSimple(500);
        }
    }

    public void StopStopTime()
    {
        if (isStopTime)
        {
            isStopTime = false;
            timeManager.EndAlterTime(true);
            postProcessingController.SetChromaticAberration(false);
            postProcessingController.SetLensDistortion(0.1f);
            postProcessingController.SetSaturation(0);
            lr.enabled = false;
            AudioManager.instance.PlayFromManager(9);
        }
    }

    public void DrawTrajectory(Rigidbody2D rigidbody, Vector2 direction)
    {
        Vector2[] trajectory = Plot(rigidbody, rigidbody.gameObject.transform.position, direction, 500);

        lr.positionCount = trajectory.Length;

        Vector3[] positions = new Vector3[trajectory.Length];
        for (int i = 0; i < trajectory.Length; i++)
        {
            positions[i] = trajectory[i];
        }
        lr.SetPositions(positions);
    }

    Vector2[] Plot(Rigidbody2D rigidbody, Vector2 pos, Vector2 velocity, int steps)
    {
        Vector2[] results = new Vector2[steps];

        float timestep = Time.fixedDeltaTime / Physics2D.velocityIterations;
        Vector2 gravityAccel = Physics2D.gravity * rigidbody.gravityScale * timestep * timestep;

        float drag = 1f - timestep * rigidbody.drag;
        Vector2 moveStep = velocity * timestep;

        for (int i = 0; i < steps; i++)
        {
            moveStep += gravityAccel;
            moveStep *= drag;
            pos += moveStep;
            results[i] = pos;
        }
        return results;
    }
}
