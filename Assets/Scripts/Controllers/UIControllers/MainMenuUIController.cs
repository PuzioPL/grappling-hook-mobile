﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUIController : MonoBehaviour
{
    public Button startButton;
    public Button shopButton;
    public Button optionsButton;
    public Button aboutButton;
    public Button privacyButton;

    void Start()
    {
        startButton.onClick.AddListener(() =>
        {
            AudioManager.instance.PlayFromManager(0);
            EventBroker.CallOnStartButton();
        });

        shopButton.onClick.AddListener(() =>
        {
            AudioManager.instance.PlayFromManager(0);
            EventBroker.CallOnPlayerDeath();
        });

        optionsButton.onClick.AddListener(() =>
        {
            AudioManager.instance.PlayFromManager(0);
            EventBroker.CallOnOptionsButton();
        });

        aboutButton.onClick.AddListener(() =>
        {
            AudioManager.instance.PlayFromManager(0);
            EventBroker.CallOnAboutButton();
        });

        privacyButton.onClick.AddListener(() =>
        {
            Application.OpenURL("https://dataoptout-ui-prd.uca.cloud.unity3d.com/?token=kac68hojvfok743van6p0abe0853mcjv6ucbnqhhrtvbbe3u");
        });
    }
}
