﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldenEdge : InteractableObjectBase
{
    public Color scrollingTextColor;

    private bool isGolden = true;
    private SpriteRenderer sr;

    private void Awake()
    {
        sr = gameObject.GetComponent<SpriteRenderer>();
        SetGolden(true);

        EventBroker.OnRespawn += () => SetGolden(true);
    }

    public override void OnPlayerCollision(PlayerController player)
    {
    }

    public override void OnHookCollision(HookScript hook, float distance, Vector2 hookPosition)
    {
        if (isGolden)
        {
            EventBroker.CallOnAddCurrency(CurrencyReward, distance, hookPosition, scrollingTextColor);
            SetGolden(false);
            AudioManager.instance.PlayFromManager(17);
        }
        else
        {
            EventBroker.CallOnAddCurrency(CurrencyReward, distance, hookPosition);
        }
    }

    void SetGolden(bool state)
    {
        if (state)
        {
            isGolden = true;
            CurrencyReward = 500;
            sr.color = new Color(1, 1, 0, 1);
        }
        else
        {
            isGolden = false;
            CurrencyReward = 10;
            sr.color = new Color(0.4f, 0.4f, 0.4f, 1);
        }
    }
}
