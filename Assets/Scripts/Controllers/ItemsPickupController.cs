﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsPickupController : MonoBehaviour
{
    public UIController uiController;

    public ItemPickupEntity rightItem;
    public ItemPickupEntity leftItem;
    public ItemPickupEntity topItem;

    public Transform containerRightItem;
    public Transform containerLeftItem;
    public Transform containerTopItem;

    [HideInInspector] public ItemPickup leftItemInstance;

    void Start()
    {
        if (!rightItem.isPickedUp)
        {
            Instantiate(rightItem.itemPrefab, containerRightItem);
        }

        if (!leftItem.isPickedUp)
        {
            leftItemInstance = Instantiate(leftItem.itemPrefab, containerLeftItem);
        }

        if (!topItem.isPickedUp)
        {
            Instantiate(topItem.itemPrefab, containerTopItem);
        }

        EventBroker.OnItemPickup += (itemSide, showPopup) =>
        {
            SetItemPicked(itemSide, true, showPopup);
        };
    }

    public void SetItemPicked(ItemSide itemSide, bool isPickedUp, bool showPopup)
    {
        switch (itemSide)
        {
            case ItemSide.Right:
                rightItem.isPickedUp = isPickedUp;
                uiController.shopUIController.ShowRightItemIcon(isPickedUp, showPopup);
                if (showPopup)
                {
                    FindObjectOfType<PopupController>().CreatePopup(0);
                }

                break;
            case ItemSide.Left:
                leftItem.isPickedUp = isPickedUp;
                uiController.shopUIController.ShowLeftItemIcon(isPickedUp, showPopup);
                if (showPopup)
                {
                    FindObjectOfType<PopupController>().CreatePopup(1);
                }
                break;
            case ItemSide.Top:
                topItem.isPickedUp = isPickedUp;

                if (isPickedUp)
                {
                    if (showPopup)
                    {
                        FindObjectOfType<PopupController>().CreatePopup(2);

                        // zagraj fanfary, a po 7 sekundach zmień muzykę na "intr0"
                        AudioManager.instance.StopMusic();
                        AudioManager.instance.PlayFanfare();
                        AudioManager.instance.SetMusicSource(1);
                        LeanTween.delayedCall(7, () =>
                        {
                            AudioManager.instance.StopFanfare();
                            AudioManager.instance.PlayMusic();
                        });
                    }
                }
                break;
            default:
                break;
        }
    }
}

[System.Serializable]
public class ItemPickupEntity
{
    public ItemPickup itemPrefab;
    public bool isPickedUp = false;
}

[System.Serializable]
public enum ItemSide
{
    Right,
    Left,
    Top
}