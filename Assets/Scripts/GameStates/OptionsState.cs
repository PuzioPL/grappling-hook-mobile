using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsState : GameStateBase
{
    public OptionsState(GameStateController controller) : base(controller)
    {
    }

    public override void OnEnterState()
    {
        stateController.uiController.SwitchUI(GameStatesEnum.Options);
    }

    public override void OnExitState()
    {
        
    }

    public override void BackButtonEvent()
    {
        stateController.TransitionToState(stateController.MainMenuState);
    }
}
