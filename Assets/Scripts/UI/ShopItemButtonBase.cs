﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class ShopItemButtonBase : MonoBehaviour
{
    public Button button;
    [HideInInspector] public ShopItemBase shopItem;
    public Image buttonImage;
    public Image icon;
    public Text mainText;
    public Text priceText;
    public Image coverLayer;
    protected float coverCurrentOpacity;
    protected float mainCurrentOpacity;

    public void AssignShopItem(ShopItemBase shopItemUsed)
    {
        shopItem = shopItemUsed;
        icon.sprite = shopItemUsed.Icon;
        UpdateText();
    }
    public void UpdateText()
    {
        mainText.text = shopItem.GetButtonMainText();

        if (priceText != null)
        {
            priceText.text = shopItem.GetButtonPriceText();
        }
    }

    public void HideOrShow()
    {
        if (shopItem.IsHidden)
        {
            button.gameObject.SetActive(false);
        }
        else
        {
            button.gameObject.SetActive(true);
        }
    }

    public abstract void SetSelected(bool isSelect, Button buyButton = null);

    public abstract void SetOpaque(int currentCurrency, Button buyButton = null);

    public abstract void OnClicked(PlayerController player);
    public virtual void DeselectItem() { }
}
