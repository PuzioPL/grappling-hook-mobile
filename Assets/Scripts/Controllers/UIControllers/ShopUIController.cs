﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopUIController : MonoBehaviour
{
    public UIController uiController;
    public PlayerController player;
    public ShopController shopController;
    public GameObject shopUpgradesContainer;
    public GameObject statIncreaseUpgradeBackground;
    public GameObject abilityUpgradeBackbround;
    public GameObject utilityUpgradeBackground;
    public GameObject zonesUtilityUpgradeBackground;
    public GameObject skinsBackground;
    private int counterUpgradeShopType = 0;
    public Button leftShopButton;
    public Button rightShopButton;
    public ShopItemButtonBase UpgradeButtonPrefab;
    public ShopItemButtonBase SkinButtonPrefab;
    private List<ShopItemButtonBase> shopItemButtons;
    public TextMeshProUGUI upgradeInfoText;
    public Button buyUpgradeButton;
    public Button retryButton;
    public PickupItemButton rightItemIcon;
    public PickupItemButton leftItemIcon;
    public TextMeshProUGUI currencyText;

    public RewardedAdsScript rewardedAdsScript;
    public AdsPlusButton adPlusButton;
    public AdsWindow adsWindow;

    void Start()
    {
        retryButton.onClick.AddListener(() => {
            uiController.RefreshUIElements();
            EventBroker.CallOnRetryButton();
            AudioManager.instance.PlayFromManager(0);
        });

        shopItemButtons = new List<ShopItemButtonBase>();

        foreach (var shopItem in shopController.shopItems)
        {
            GameObject parent = null;
            ShopItemButtonBase button = null;

            switch (shopItem.Type)
            {
                case ShopItemTypeEnum.StatIncrease:
                    parent = statIncreaseUpgradeBackground;
                    button = UpgradeButtonPrefab;
                    break;
                case ShopItemTypeEnum.Ability:
                    parent = abilityUpgradeBackbround;
                    button = UpgradeButtonPrefab;
                    break;
                case ShopItemTypeEnum.Utility:
                    parent = utilityUpgradeBackground;
                    button = UpgradeButtonPrefab;
                    break;
                case ShopItemTypeEnum.ZonesUtility:
                    parent = zonesUtilityUpgradeBackground;
                    button = UpgradeButtonPrefab;
                    break;
                case ShopItemTypeEnum.SkinPlayer:
                case ShopItemTypeEnum.SkinHook:
                    parent = skinsBackground;
                    button = SkinButtonPrefab;
                    break;
                default:
                    break;
            }

            var newButton = Instantiate(button, parent.transform);
            newButton.AssignShopItem(shopItem);
            newButton.HideOrShow();
            newButton.button.onClick.AddListener(() => ShopItemButtonClicked(newButton));

            shopItemButtons.Add(newButton);

            EventBroker.OnMultiplyCurrency += (multiplier) => OpacityOnUpgrades();
        }

        rightShopButton.onClick.AddListener(() => ShopNavigationClicked(-1));
        leftShopButton.onClick.AddListener(() => ShopNavigationClicked(1));

        shopController.OnRevealItems += ShowHiddenItems;

        adPlusButton.Initialize(() => adsWindow.gameObject.SetActive(true));
        adsWindow.Initialize(() => rewardedAdsScript.ShowRewardedVideo());
        adsWindow.isChoseWatchAd += (isChoseWatchAd) =>
        {
            adPlusButton.SetPulsing(false);

            if (isChoseWatchAd)
            {
                adPlusButton.gameObject.SetActive(false);
                adsWindow.gameObject.SetActive(false);
            }
        };
    }

    public void InitializeAds()
    {
        var isAdReady = rewardedAdsScript.IsAdReady();
        adPlusButton.gameObject.SetActive(isAdReady);

        if (isAdReady)
        {
            var reward = player.currencyData.CurrentCurrency * 0.3f;
            adsWindow.SetRewardValue((int)(reward));
            rewardedAdsScript.currentReward = (int)reward;
        }
    }

    void ShowHiddenItems()
    {
        foreach (var itemButton in shopItemButtons)
        {
            itemButton.HideOrShow();
        }
    }

    public void PickupItemButtonClicked(PickupItemButton button)
    {
        DeselectItems();
        DeselectSkinsAndSelectOne(null);

        button.Select();

        upgradeInfoText.gameObject.SetActive(true);
        upgradeInfoText.text = button.Info;
        buyUpgradeButton.gameObject.SetActive(false);
        AudioManager.instance.PlayFromManager(14);
    }

    void ShopItemButtonClicked(ShopItemButtonBase shopItemButton)
    {
        DeselectItems();

        DeselectSkinsAndSelectOne(shopItemButton);

        // zmień description
        buyUpgradeButton.onClick.RemoveAllListeners();
        upgradeInfoText.gameObject.SetActive(true);
        upgradeInfoText.text = shopItemButton.shopItem.Info;
        buyUpgradeButton.onClick.AddListener(() => BuyClicked(shopItemButton));

        AudioManager.instance.PlayFromManager(14);
    }

    void DeselectItems()
    {
        rightItemIcon.Deselect();
        leftItemIcon.Deselect();
    }

    void DeselectSkinsAndSelectOne(ShopItemButtonBase shopItemButton)
    {
        foreach (var singleButton in shopItemButtons)
        {
            singleButton.SetSelected(false);
        }

        if (shopItemButton == null)
        {
            foreach (var singleButton in shopItemButtons)
            {
                BorderSelectedSkin(singleButton);
            }
            return;
        }

        if (shopItemButton.shopItem.IsMaxed() && !shopItemButton.shopItem.IsPlaceholder)
        {
            foreach (var singleButton in shopItemButtons)
            {
                if (singleButton.shopItem.Type == shopItemButton.shopItem.Type)
                { 
                    singleButton.DeselectItem();
                }
                BorderSelectedSkin(singleButton);
            }
            shopItemButton.OnClicked(player);
        }
        else
        {
            foreach (var singleButton in shopItemButtons)
            {
                BorderSelectedSkin(singleButton);
            }
        }

        shopItemButton.SetSelected(true, buyUpgradeButton);
    }

    void BorderSelectedSkin(ShopItemButtonBase shopButton)
    {
        if (shopButton.shopItem.IsSelectedValue())
        {
            shopButton.OnClicked(player);
        }
        else
        {
            shopButton.DeselectItem();
        }
    }

    void BuyClicked(ShopItemButtonBase shopItemButton)
    {
        var shopItem = shopItemButton.shopItem;

        if (!shopItem.IsMaxed() && shopItem.IsAffordable(player.currencyData.CurrentCurrency))
        {
            player.currencyData.ApplyCurrentCurrency(-shopItem.GetPrice());
            shopItem.Buy(player);
            DeselectSkinsAndSelectOne(shopItemButton);
            uiController.SetCurrency(player.currencyData.CurrentCurrency, 0.5f);

            shopItemButton.UpdateText();
            shopItemButton.SetOpaque(player.currencyData.CurrentCurrency, buyUpgradeButton);
            AudioManager.instance.PlayFromManager(15);
        }
        else
        {
            AudioManager.instance.PlayFromManager(16);
        }

        OpacityOnUpgrades();
    }

    public void OpacityOnUpgrades()
    {
        foreach (var shopItemButton in shopItemButtons)
        {
            BorderSelectedSkin(shopItemButton);
            shopItemButton.SetOpaque(player.currencyData.CurrentCurrency);
        }
    }
    void ShopNavigationClicked(int direction)
    {
        counterUpgradeShopType += direction;

        shopUpgradesContainer.transform.localPosition = (Vector2)shopUpgradesContainer.transform.localPosition + new Vector2((direction * 667), 0);
        switch (counterUpgradeShopType)
        {
            case -2:
                rightShopButton.gameObject.SetActive(false);
                break;
            case -1:
                leftShopButton.gameObject.SetActive(true);
                rightShopButton.gameObject.SetActive(true);
                break;
            case 1:
                leftShopButton.gameObject.SetActive(true);
                rightShopButton.gameObject.SetActive(true);
                break;
            case 2:
                leftShopButton.gameObject.SetActive(false);
                break;
        }
        AudioManager.instance.PlayFromManager(14);
    }

    public void ShowRightItemIcon(bool isShow, bool showPopup)
    {
        if (isShow)
        {
            rightItemIcon.gameObject.SetActive(true);
        }
        else
        {
            rightItemIcon.gameObject.SetActive(false);
        }
    }

    public void ShowLeftItemIcon(bool isShow, bool showPopup)
    {
        if (isShow)
        {
            leftItemIcon.gameObject.SetActive(true);
        }
        else
        {
            leftItemIcon.gameObject.SetActive(false);
        }
    }

    public void RefreshShopUI()
    {
        DeselectItems();
        buyUpgradeButton.gameObject.SetActive(false);
        upgradeInfoText.gameObject.SetActive(false);

        foreach (var singleButton in shopItemButtons)
        {
            singleButton.SetSelected(false);
        }
    }
}
