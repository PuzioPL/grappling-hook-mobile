﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayUIController : MonoBehaviour
{
    public UIController uiController;
    public PlayerController player;

    [Header("Play UI")]
    public Button pauseButton;
    public Button severButton;
    public TextMeshProUGUI chargesText;
    public Slider slowmoSlider;
    public Slider multiplierSlider;
    public Text multiplierCounter;
    public GameObject countersBackground;
    public GameObject countersCover;
    public TextMeshProUGUI currencyText;
    [SerializeField] public AbilityButton abilityButton1;
    [SerializeField] public AbilityButton abilityButton2;
    private List<AbilityButton> abilitiesButtons;

    private void Awake()
    {
        EventBroker.OnApplyCharges += SetCharges;
        EventBroker.OnSetGrappleCount += SetGrappleCount;
        EventBroker.OnGrappleCountFlash += () => StartCoroutine(FlashGrappleCount());
    }

    void Start()
    {
        pauseButton.onClick.AddListener(() =>
        {
            EventBroker.CallOnPauseButton();
            AudioManager.instance.PlayFromManager(0);
        });

        severButton.onClick.AddListener(() => EventBroker.CallOnGrappleSever());

        abilityButton1.AssignAbility(uiController.shopController.ability1, player);
        abilityButton2.AssignAbility(uiController.shopController.ability2, player);
        abilitiesButtons = new List<AbilityButton>() { abilityButton1, abilityButton2 };
    }
    
    void Update()
    {
        foreach (var abilityButton in abilitiesButtons)
        {
            if (abilityButton.ability.IsActive && abilityButton.ability.NextTimeToUse > Time.time)
            {
                var timeDifference = abilityButton.ability.NextTimeToUse - Time.time;

                abilityButton.cooldown.fillAmount = (timeDifference / abilityButton.ability.CurrentCooldown);

                if (abilityButton.ability.NextTimeToUse - Time.time < 0.05f)
                {
                    abilityButton.SetOpacity(0);
                }
            }
        }
    }

    public void RefreshPlayUI()
    {
        SetSlowmoBar(1);

        foreach (var abilityButton in abilitiesButtons)
        {
            if (abilityButton.ability.IsActive && !abilityButton.ability.IsAlreadyAdded)
            {
                abilityButton.button.gameObject.SetActive(true);
                abilityButton.ability.IsAlreadyAdded = true;
            }
            abilityButton.cooldown.fillAmount = 0;
            abilityButton.SetOpacity(0);
        }

        if (player.grapplingGun.isGrappleExchangeActive)
        {
            var imagesCover = countersCover.transform.GetComponentsInChildren<Image>();

            foreach (var image in imagesCover)
            {
                image.enabled = false;
            }

            countersCover.SetActive(true);
            countersBackground.SetActive(true);

            var images = countersBackground.transform.GetComponentsInChildren<Image>();

            foreach (var background in images)
            {
                background.enabled = false;
            }

            for (int i = 0; i < player.grapplingGun.currentGrappleToChargeExchange; i++)
            {
                images[i].enabled = true;
            }
        }
        else
        {
            countersCover.SetActive(false);
            countersBackground.SetActive(false);
        }
    }

    public void SetSlowmoBar(float percent)
    {
        slowmoSlider.value = percent;
    }

    public void SetMultiplierBar(int currentMultiplier, float percent)
    {
        multiplierSlider.value = percent;
        multiplierCounter.text = $"x{currentMultiplier}";
    }

    private void SetCharges(int charges)
    {
        chargesText.text = $"Charges: { charges }";

        if (charges == 0)
        {
            chargesText.color = Color.red;
        }
        else
        {
            chargesText.color = Color.white;
        }
    }

    private void SetGrappleCount()
    {
        if (player.grapplingGun.isGrappleExchangeActive)
        {
            var images = countersCover.transform.GetComponentsInChildren<Image>();

            foreach (var image in images)
            {
                image.enabled = false;
            }

            for (int i = 0; i < player.grapplingGun.grappleCount; i++)
            {
                images[i].enabled = true;
            }
        }
    }

    IEnumerator FlashGrappleCount()
    {
        if (player.grapplingGun.isGrappleExchangeActive)
        {
            var images = countersCover.transform.GetComponentsInChildren<Image>();

            SetAllGrappleCountersVisibility(images, false);
            yield return new WaitForSeconds(0.2f);

            SetAllGrappleCountersVisibility(images, true);
            yield return new WaitForSeconds(0.2f);

            SetAllGrappleCountersVisibility(images, false);
            yield return new WaitForSeconds(0.2f);

            SetAllGrappleCountersVisibility(images, true);
            yield return new WaitForSeconds(0.2f);

            SetAllGrappleCountersVisibility(images, false);
            yield return new WaitForSeconds(0.2f);

            SetGrappleCount();
        }
    }

    private void SetAllGrappleCountersVisibility(Image[] images, bool isEnabled)
    {
        for (int i = 0; i < player.grapplingGun.currentGrappleToChargeExchange; i++)
        {
            images[i].enabled = isEnabled;
        }
    }
}
