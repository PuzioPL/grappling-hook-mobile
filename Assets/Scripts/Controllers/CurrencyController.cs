﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyController : MonoBehaviour
{
    public UIController uiController;

    public CurrencyData_SO currencyData;

    public int baseMaxMultiplier;
    public int currentMaxMultiplier;
    public int currentMultiplier;

    public int baseMaxMultiplierDuration;
    public int currentMaxMultiplierDuration;
    public float multiplierTimeLeft;

    private bool isAlreadyFinishedUpdating = true;

    void Awake()
    {
        currencyData.currencyController = this;

        EventBroker.OnAddCurrency += AddCurrency;
        EventBroker.OnMultiplyCurrency += MultiplyCurrency;

        EventBroker.CallOnSetUICurrency(currencyData.GetCurrentCurrency());
    }

    private void Update()
    {
        var previousTimeLeft = 0f;

        if (multiplierTimeLeft > 0)
        {
            previousTimeLeft = multiplierTimeLeft;
            multiplierTimeLeft -= Time.deltaTime;
        }
        else
        {
            multiplierTimeLeft = 0;
            currentMultiplier = 1;

            // ten weird mumbo jumbo jest po to, żeby nie kazało updejtować multipliera bara, kiedy multiplier jest wyzerowany, 
            //  ale żeby zaraz przed tym wysłało SetMultiplierBar ostatni raz, co ustawi MultiplierCounter na "x1" w UI
            if (!isAlreadyFinishedUpdating)
            {
                uiController.playUIController.SetMultiplierBar(currentMultiplier, multiplierTimeLeft / currentMaxMultiplierDuration);
            }
            isAlreadyFinishedUpdating = true;
        }

        if (currentMultiplier > 1)
        {
            uiController.playUIController.SetMultiplierBar(currentMultiplier, multiplierTimeLeft / currentMaxMultiplierDuration);
        }
    }

    private void MultiplyCurrency(float multiplier)
    {
        var reward = (int)(currencyData.CurrentCurrency * multiplier);
        currencyData.ApplyCurrentCurrency(reward);
        EventBroker.CallOnSetUICurrency(currencyData.GetCurrentCurrency());
    }

    private void AddCurrency(int amount, float distance, Vector2? position, Color? color)
    {
        var reward = (amount + (int)distance) * currentMultiplier;
        //Debug.Log($"Amount: {amount}, distance: {distance}, multiplier: {currentMultiplier}, reward: {reward}");

        currencyData.ApplyCurrentCurrency(reward);
        EventBroker.CallOnSetUICurrency(currencyData.GetCurrentCurrency(), 0.5f);

        if (position != null)
        {
            EventBroker.CallOnCurrencyScrollingText(reward, position.Value, color);
            IncreasePointsMultiplier();
        }
    }

    public void IncreasePointsMultiplier()
    {
        isAlreadyFinishedUpdating = false;
        currentMultiplier += 1;

        if (currentMultiplier > currentMaxMultiplier)
            currentMultiplier = currentMaxMultiplier;

        multiplierTimeLeft = currentMaxMultiplierDuration;
    }
}
