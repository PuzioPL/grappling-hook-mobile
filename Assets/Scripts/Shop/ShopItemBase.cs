﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class ShopItemBase : ScriptableObject
{
    public int Id;
    public string Text;
    [TextArea(2,5)] public string Info;
    public Sprite Icon;
    public ShopItemTypeEnum Type;
    public bool IsPlaceholder;
    public bool IsHidden;

    public abstract string GetButtonMainText();
    public abstract string GetButtonPriceText();
    public abstract void Buy(PlayerController player);
    public abstract bool IsAffordable(int currentCurrency);
    public abstract bool IsMaxed();
    public abstract int GetPrice();
    public abstract void OnSelect(PlayerController player);
    public abstract void ReadSaveData(SaveableDataBase data = null);
    public abstract void ApplyItem(PlayerController player);
    public virtual bool IsSelectedValue() { return false; }
    public virtual void Deselect() {  }
}
