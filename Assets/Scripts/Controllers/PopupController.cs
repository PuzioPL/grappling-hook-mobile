﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupController : MonoBehaviour
{
    public Popup popupPrefab;
    public Transform popupParent;
    public List<PopupData> popupsData;

    public Popup CreatePopup(int popupNumber)
    {
        var newPopup = Instantiate(popupPrefab, popupParent);

        var chosenPopupData = popupsData[popupNumber];
        newPopup.Initialize(chosenPopupData.iconSprite, chosenPopupData.text, chosenPopupData.timeShown);
        return newPopup;
    }
}
