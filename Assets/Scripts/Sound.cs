using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sound
{
    public AudioClip clip;
    [SerializeField] private float minTimeBetweenPlays;
    [SerializeField] public float volume = 1;
    [HideInInspector] public float lastTimePlayed;

    public bool CanBePlayed()
    {
        return lastTimePlayed + minTimeBetweenPlays < Time.time;
    }
}
