﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickupItemButton : MonoBehaviour
{
    public string Info;
    [HideInInspector] public Button button;
    private Image image;

    private void Awake()
    {
        button = GetComponent<Button>();
        image = GetComponent<Image>();
    }

    public void Select()
    {
        image.color = new Color(0.2f, 0.6f, 1);
    }

    public void Deselect()
    {
        image.color = new Color(0.282353f, 0.282353f, 0.282353f, 0.6156863f);
    }
}
