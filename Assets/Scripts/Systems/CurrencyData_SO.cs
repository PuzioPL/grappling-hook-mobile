﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Currency Data", menuName = "Currency")]
public class CurrencyData_SO : ScriptableObject
{
    [SerializeField]
    public CurrencyController currencyController;
    public int CurrentCurrency;

    public int GetCurrentCurrency()
    {
        return CurrentCurrency;
    }

    public bool ApplyCurrentCurrency(int currencyAmount)
    {
        if (CheckIfAffordable(currencyAmount))
        {
            CurrentCurrency += currencyAmount;
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool CheckIfAffordable(int amount)
    {
        var tempCurrency = CurrentCurrency + amount;

        return tempCurrency >= 0;
    }
}
