﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class InteractableObjectBase : MonoBehaviour
{
    public int CurrencyReward { get; set; }
    public bool IsGrappable { get; set; } = true;

    public virtual void OnSpawn(ObjectSpawnerController controller)
    {
        Setup(controller);

        var isVertical = true;
        if (UnityEngine.Random.value >= 0.5)
            isVertical = false;

        if (isVertical)
            transform.rotation = Quaternion.Euler(0, 0, 90);
    }

    public abstract void OnPlayerCollision(PlayerController player);

    public abstract void OnHookCollision(HookScript hook, float distance, Vector2 hookPosition);

    public virtual void Setup(ObjectSpawnerController controller) { }
}
