﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowmoController : MonoBehaviour
{
    public UIController uiController;
    public PostProcessingController postProcessingController;
    public TimeManager timeManager;

    public float slowmoFactor;

    public float baseMaxSlowmoDuration;
    public float currentMaxSlowmoDuration;

    public float slowmoDurationLeft;

    public bool isSlowmo;

    private bool isPause = false;

    private void Start()
    {
        slowmoDurationLeft = currentMaxSlowmoDuration;

        EventBroker.OnPauseButton += () => isPause = true;
        EventBroker.OnPlayerDeath += () => isPause = false;
        EventBroker.OnResumeButton += () => isPause = false;
    }

    private void Update()
    {
        if (!isPause)
        {
            if (isSlowmo)
            {
                if (slowmoDurationLeft > 0)
                {
                    slowmoDurationLeft -= Time.unscaledDeltaTime;
                }
                else
                {
                    timeManager.EndAlterTime(false);
                    isSlowmo = false;
                }
            }
            else
            {
                if (Time.timeScale != 0)
                    slowmoDurationLeft += Time.unscaledDeltaTime / 2;
            }

            slowmoDurationLeft = Mathf.Clamp(slowmoDurationLeft, 0, currentMaxSlowmoDuration);

            if (slowmoDurationLeft < currentMaxSlowmoDuration)
            {
                uiController.playUIController.SetSlowmoBar((slowmoDurationLeft / currentMaxSlowmoDuration));
            }

            if (slowmoDurationLeft < 0.1)
            {
                postProcessingController.SetChromaticAberration(false);
            }
        }
    }

    public void StartSlowmo()
    {
        if (!isSlowmo)
        {
            isSlowmo = true;
            timeManager.DoAlterTime(slowmoFactor);
            postProcessingController.SetChromaticAberration(true);

            AudioManager.instance.StartLowpassSimple(1000);
        }
    }

    public void StopSlowmo()
    {
        if (isSlowmo)
        {
            timeManager.EndAlterTime(false);
            postProcessingController.SetChromaticAberration(false);
            isSlowmo = false;
        }
    }

    public void SetSlowmoBarManually()
    {
        uiController.playUIController.SetSlowmoBar((slowmoDurationLeft / currentMaxSlowmoDuration));
    }
}
