using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeScript : MonoBehaviour
{
    public Animator camAnim;

    public static CameraShakeScript instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    public void CamShake()
    {
        int rand = Random.Range(1, 5);
        camAnim.SetTrigger($"Shake{rand}");
    }
}
