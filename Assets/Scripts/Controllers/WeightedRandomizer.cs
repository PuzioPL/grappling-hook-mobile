﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// http://wiki.unity3d.com/index.php?title=WeightedRandomizer&oldid=16793
/// Troszkę zmieniłem, żeby działało pod InteractableObjectWithWeight.
/// Static class to improve readability
/// Example:
/// <code>
/// var selected = WeightedRandomizer.From(weights).TakeOne();
/// </code>
/// 
/// </summary>
public static class WeightedRandomizerStatic
{
    public static WeightedRandomizer From(List<InteractableObjectWithWeight> spawnRate)
    {
        return new WeightedRandomizer(spawnRate);
    }
}

public class WeightedRandomizer
{
    private static System.Random _random = new System.Random();
    private List<InteractableObjectWithWeight> _weights;

    /// <summary>
    /// Instead of calling this constructor directly,
    /// consider calling a static method on the WeightedRandomizer (non-generic) class
    /// for a more readable method call, i.e.:
    /// 
    /// <code>
    /// var selected = WeightedRandomizer.From(weights).TakeOne();
    /// </code>
    /// 
    /// </summary>
    /// <param name="weights"></param>
    public WeightedRandomizer(List<InteractableObjectWithWeight> weights)
    {
        _weights = weights;
    }

    /// <summary>
    /// Randomizes one item
    /// </summary>
    /// <param name="spawnRate">An ordered list withe the current spawn rates. The list will be updated so that selected items will have a smaller chance of being repeated.</param>
    /// <returns>The randomized item.</returns>
    public InteractableObjectBase TakeOne()
    {
        // Sorts the spawn rate list
        var sortedSpawnRate = Sort(_weights);

        // Sums all spawn rates
        int sum = 0;
        foreach (var spawn in _weights)
        {
            sum += spawn.currentWeight;
        }

        // Randomizes a number from Zero to Sum
        int roll = _random.Next(0, sum);

        // Finds chosen item based on spawn rate
        InteractableObjectBase selected = sortedSpawnRate[sortedSpawnRate.Count - 1].interactableObject;
        foreach (var spawn in sortedSpawnRate)
        {
            if (roll < spawn.currentWeight)
            {
                selected = spawn.interactableObject;
                break;
            }
            roll -= spawn.currentWeight;
        }

        // Returns the selected item
        return selected;
    }

    private List<InteractableObjectWithWeight> Sort(List<InteractableObjectWithWeight> weights)
    {
        var list = new List<InteractableObjectWithWeight>(weights);

        // Sorts the Spawn Rate List for randomization later
        list.Sort(
            delegate (InteractableObjectWithWeight firstPair,
                     InteractableObjectWithWeight nextPair)
            {
                return firstPair.currentWeight.CompareTo(nextPair.currentWeight);
            }
         );

        return list;
    }
}