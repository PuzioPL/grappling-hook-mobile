using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionArrow : MonoBehaviour
{
    public Transform player;
    public float pointSpeed;
    public float arrowDistanceFromPlayer;
    public float minDistanceFromTarget;
    public float minArrowSize;
    public float maxArrowSize;
    public float timeBetweenFlashes;
    public float flashingDistance;

    private float nextFlashTime;
    private Transform target;
    private float colorHue;
    private SpriteRenderer spriteRenderer;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        SetArrowRotation();
        SetArrowPosition();

        var distanceToTarget = Vector2.Distance(player.position, target.position);

        SetArrowColorAndScale(distanceToTarget);

        if (distanceToTarget < flashingDistance)
        {
            if (Time.time >= nextFlashTime)
            {
                StartCoroutine(Flash());
            }
        }
    }

    IEnumerator Flash()
    {
        nextFlashTime = Time.time + timeBetweenFlashes;

        spriteRenderer.enabled = false;
        yield return new WaitForSeconds(timeBetweenFlashes / 2);
        spriteRenderer.enabled = true;
    }

    public void SetupArrow(Transform setTarget, Color setColor)
    {
        target = setTarget;

        var satur = 0f;
        var val = 0f;
        Color.RGBToHSV(setColor, out colorHue, out satur, out val);
        spriteRenderer.enabled = true;
    }

    void SetArrowRotation()
    {
        var pointDirection = target.position - transform.position;

        float angleArrow = Mathf.Atan2(pointDirection.y, pointDirection.x) * Mathf.Rad2Deg;
        Quaternion rotationArrow = Quaternion.AngleAxis(angleArrow - 90, Vector3.forward);

        transform.rotation = Quaternion.Slerp(transform.rotation, rotationArrow, pointSpeed * Time.deltaTime);
    }

    void SetArrowPosition()
    {
        var directionFromPlayer = target.position - transform.position;
        float anglePlayer = Mathf.Atan2(directionFromPlayer.y, directionFromPlayer.x);

        var xPos = Mathf.Cos(anglePlayer) * arrowDistanceFromPlayer;
        var yPos = Mathf.Sin(anglePlayer) * arrowDistanceFromPlayer;

        transform.position = (Vector2)player.position + new Vector2(xPos, yPos);
    }

    void SetArrowColorAndScale(float distanceToTarget)
    {
        if (distanceToTarget > minDistanceFromTarget)
            return;

        var inverselyProportionate = Mathf.InverseLerp(minDistanceFromTarget, 0, distanceToTarget);

        var newSaturationValue = Mathf.Lerp(0, 1, inverselyProportionate);
        var newColor = Color.HSVToRGB(colorHue, newSaturationValue, 1);
        spriteRenderer.color = newColor;

        var newScale = Mathf.Lerp(minArrowSize, maxArrowSize, inverselyProportionate);
        gameObject.transform.localScale = new Vector3(newScale, newScale, 1);
    }
}
