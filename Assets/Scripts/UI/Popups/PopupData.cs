﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PopupData", menuName = "Popup")]
public class PopupData : ScriptableObject
{
    public Sprite iconSprite;
    public string text;
    public float timeShown = 3;
}
