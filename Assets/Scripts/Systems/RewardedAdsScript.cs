using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;

[System.Serializable]
public class RewardedAdsScript : MonoBehaviour, IUnityAdsListener
{
    string GooglePlayId = "4063269";
    string mySurfacingId = "Rewarded_Android";
    bool testMode = false;

    float lastTimeAdShown;
    float timeBetweenAds;

    public int currentReward;

    // Initialize the Ads listener and service:
    void Start()
    {
        Advertisement.AddListener(this);
        Advertisement.Initialize(GooglePlayId, testMode);
        lastTimeAdShown = -180;
        timeBetweenAds = 180; // 3 mins
    }

    public void ShowRewardedVideo()
    {
        // Check if UnityAds ready before calling Show method:
        if (Advertisement.IsReady(mySurfacingId))
        {
            Advertisement.Show(mySurfacingId);
        }
        else
        {
            Debug.Log("Rewarded video is not ready at the moment! Please try again later!");
        }
    }

    public bool IsAdReady()
    {
        if (lastTimeAdShown + timeBetweenAds <= Time.time)
        {
            return Advertisement.IsReady(mySurfacingId);
        }
        else
        {
            return false;
        }

    }

    public void OnUnityAdsDidFinish(string surfacingId, ShowResult showResult)
    {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished)
        {
            // Reward the user for watching the ad to completion.
            Debug.Log("Reward granted");
            lastTimeAdShown = Time.time;
            EventBroker.CallOnAddCurrency((int)currentReward, 0, null);
        }
        else if (showResult == ShowResult.Skipped)
        {
            // Do not reward the user for skipping the ad.
            Debug.Log("Reward NOT granted");
        }
        else if (showResult == ShowResult.Failed)
        {
            Debug.LogWarning("The ad did not finish due to an error.");
        }
        AudioManager.instance.TurnMasterVolume(true);
    }

    public void OnUnityAdsReady(string surfacingId)
    {
        // If the ready Ad Unit or legacy Placement is rewarded, show the ad:
        if (surfacingId == mySurfacingId)
        {
            // Optional actions to take when theAd Unit or legacy Placement becomes ready (for example, enable the rewarded ads button)
        }
    }

    public void OnUnityAdsDidError(string message)
    {
        // Log the error.
    }

    public void OnUnityAdsDidStart(string surfacingId)
    {
        // Optional actions to take when the end-users triggers an ad.
    }

    // When the object that subscribes to ad events is destroyed, remove the listener:
    public void OnDestroy()
    {
        Advertisement.RemoveListener(this);
    }
}
