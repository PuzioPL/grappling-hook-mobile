using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingParticles : MonoBehaviour
{
    public Transform cam;
    public float parallaxValue;

    void Update()
    {
        transform.position = (Vector2)(cam.position * parallaxValue) + new Vector2(0, 2000);
    }
}
