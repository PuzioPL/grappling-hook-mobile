﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UpgradeData : SaveableDataBase
{
    public int currentLevel;

    public UpgradeData(int upgradeId, int upgradeLevel)
    {
        id = upgradeId;
        currentLevel = upgradeLevel;
    }
}
