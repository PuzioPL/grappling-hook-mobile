﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public FloatingJoystick Joystick;
    public PlayerController player;
    public ShopController shopController;

    [Header("State UI's")]
    public GameObject MainMenuUI;
    public GameObject PlayUI;
    public GameObject ShopUI;
    public GameObject PauseUI;
    public GameObject OptionsUI;
    public GameObject AboutUI;

    [Header("Main Menu UI")]
    public MainMenuUIController mainMenuController;

    [Header("Pause UI")]
    public PauseUIController pauseUIController;

    [Header("Play UI")]
    public PlayUIController playUIController;

    [Header("Shop UI")]
    public ShopUIController shopUIController;

    public GameObject blackUIBackground;

    private int targetDisplayedCurrency;
    private int currentDisplayedCurrency;
    private int differenceCurrency;
    private float changeTime;

    private void Start()
    {
        SetCurrency(player.currencyData.CurrentCurrency, 1);

        RefreshUIElements();

        EventBroker.OnSetUICurrency += SetCurrency;
        EventBroker.OnCurrencyScrollingText += (amount, position, color) =>
        {
            ScrollingTextController.instance.CreateWorldText(position, $"+{amount}", color);
        };
    }

    public void RefreshUIElements()
    {
        shopUIController.RefreshShopUI();
        playUIController.RefreshPlayUI();
    }

    public void SetCurrency(int currency, float changeTimee)
    {
        changeTime = changeTimee;
        currentDisplayedCurrency = targetDisplayedCurrency;
        targetDisplayedCurrency = currency;
        differenceCurrency = targetDisplayedCurrency - currentDisplayedCurrency;
    }

    void Update()
    {
        // odliczanie currency
        if (currentDisplayedCurrency != targetDisplayedCurrency)
        {
            if (differenceCurrency >= 0)
            {
                // zlicza w górę
                currentDisplayedCurrency += Mathf.Max((int)(((differenceCurrency) / changeTime) * Time.deltaTime), 1);
                currentDisplayedCurrency = (int)Mathf.Clamp(currentDisplayedCurrency, 0f, targetDisplayedCurrency);
            }
            else
            {
                // zlicza w dół
                currentDisplayedCurrency += Mathf.Min((int)(((differenceCurrency) / changeTime) * Time.deltaTime), -1);
                currentDisplayedCurrency = (int)Mathf.Clamp(currentDisplayedCurrency, targetDisplayedCurrency, 999999);
            }

            shopUIController.currencyText.text = $"{currentDisplayedCurrency}$";
            playUIController.currencyText.text = $"{currentDisplayedCurrency}$";
        }
    }

    public void SetJoystick(bool isSet)
    {
        Joystick.gameObject.SetActive(isSet);
    }

    public void SwitchUI(GameStatesEnum state, float delay = 0)
    {
        DisableUI();

        StartCoroutine(ShowUIWithDelay(state, delay));
    }

    public void DisableUI()
    {
        MainMenuUI.SetActive(false);
        PlayUI.SetActive(false);
        ShopUI.SetActive(false);
        PauseUI.SetActive(false);
        OptionsUI.SetActive(false);
        AboutUI.SetActive(false);
    }

    IEnumerator ShowUIWithDelay(GameStatesEnum state, float delay)
    {
        yield return new WaitForSeconds(delay);
        switch (state)
        {
            case GameStatesEnum.MainMenu:
                blackUIBackground.SetActive(true);
                MainMenuUI.SetActive(true);
                break;
            case GameStatesEnum.Play:
                blackUIBackground.SetActive(false);
                PlayUI.SetActive(true);
                break;
            case GameStatesEnum.Pause:
                blackUIBackground.SetActive(true);
                PauseUI.SetActive(true);
                break;
            case GameStatesEnum.Shop:
                blackUIBackground.SetActive(true);
                ShopUI.SetActive(true);
                shopUIController.InitializeAds();
                break;
            case GameStatesEnum.Options:
                blackUIBackground.SetActive(true);
                OptionsUI.SetActive(true);
                break;
            case GameStatesEnum.About:
                blackUIBackground.SetActive(true);
                AboutUI.SetActive(true);
                break;
            default:
                yield return null;
                break;
        }
    }
}
