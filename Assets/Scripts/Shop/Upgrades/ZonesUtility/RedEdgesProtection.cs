﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RedEdgesProtection", menuName = "Upgrade/Red Edges Protection")]
public class RedEdgesProtection : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        if (CurrentLevel > 0)
            player.burnMeterCanvas.IncreaseMaxBurnValue();
        else
            player.burnMeterCanvas.DecreaseMaxBurnValue();
    }
}
