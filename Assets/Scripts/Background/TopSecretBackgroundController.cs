using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopSecretBackgroundController : MonoBehaviour
{
    public GameObject topBackgroundHolder;

    private bool isEnabled;

    void Start()
    {
        SetBackground(true);
    }

    void Update()
    {
        if (transform.parent.parent.position.y < 2050)
        {
            if (isEnabled)
            {
                SetBackground(false);
            }
        }
        else
        {
            if (!isEnabled)
            {
                SetBackground(true);
            }
        }
    }

    void SetBackground(bool isOn)
    {
        isEnabled = isOn;
        topBackgroundHolder.SetActive(isEnabled);
    }
}
