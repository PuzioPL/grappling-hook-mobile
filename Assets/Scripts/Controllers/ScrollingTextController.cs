using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingTextController : MonoBehaviour
{
    public ScrollingText scrollTextPrefab;

    public static ScrollingTextController instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
            return;
        }
    }

    public void CreateWorldText(Vector2 position, string text, Color? color)
    {
        var scrollingText = Instantiate(scrollTextPrefab, position, Quaternion.identity);
        scrollingText.Setup(text, color);
    }
}
