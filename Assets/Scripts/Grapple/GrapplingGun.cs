﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingGun : MonoBehaviour
{
    public SmoothFollowTarget smoothFollowCameraHolder;
    public CameraSizeOnMove cameraOnSizeMove;
    public SlowmoController slowmoController;
    public StopTimeController stopTimeController;

    public Camera cam;
    private Vector2 grapplePoint;
    public PlayerController player;
    public float maxDistance;
    private SpringJoint2D joint;
    public float fireRate;
    public HookScript hookPrefab;
    public HookScript hookInstance;
    public float baseHookSpeed;
    public float currentHookSpeed;
    public float baseGrappleLimit;
    public CircleCollider2D grappleLimit;
    public float baseJointFrequency;
    public float currentJointFrequency;
    public int baseMaxGrappleCharges;
    public int currentMaxGrappleCharges;
    public int grappleCharges;
    public Joystick joystick;
    public LineRenderer lineRenderer;
    public SpriteRenderer targetDot;

    public bool isGrappleExchangeActive;
    public float baseGrappleToChargeExchange;
    public float currentGrappleToChargeExchange;
    public float grappleCount;
    public GameObject stopTimeParticles;

    private void Start()
    {
        grappleCharges = currentMaxGrappleCharges;

        EventBroker.OnGrappleSever += () => SeverGrapple(true);
        EventBroker.OnJoystickUp += GrappleJoystickUp;
        EventBroker.OnJoystickDown += JoystickDown;

        EventBroker.CallOnApplyCharges(currentMaxGrappleCharges);
    }

    void Update()
    {
        if (joystick.Horizontal != 0 || joystick.Vertical != 0)
        {
            smoothFollowCameraHolder.positionOffset = new Vector2(-joystick.Horizontal, -joystick.Vertical).normalized * 10;

            if (hookInstance == null)
            {
                slowmoController.StartSlowmo();
                if (slowmoController.slowmoDurationLeft > 0)
                {
                    cameraOnSizeMove.sizeOffset = -3;
                }
                else
                {
                    cameraOnSizeMove.sizeOffset = 0;
                }
            }

            if (joint == null)
            {
                lineRenderer.enabled = true;
                targetDot.enabled = true;
                DrawPredictedLine();
            }
        }
        else
        {
            smoothFollowCameraHolder.positionOffset = Vector2.zero;
            cameraOnSizeMove.sizeOffset = 0;
            slowmoController.StopSlowmo();
        }

        if (joint != null && stopTimeController.isActive)
        {
            if (joystick.Horizontal != 0 || joystick.Vertical != 0)
            {
                stopTimeController.StartStopTime();
                stopTimeController.DrawTrajectory(player.rig2d, -joystick.Direction * 200);
            }
        }

        if (Input.GetMouseButtonDown(1) && hookInstance != null)
        {
            SeverGrapple();
        }
    }

    void JoystickDown()
    {
        if (hookInstance == null || !hookInstance.isHooked)
        {
            AudioManager.instance.PlayFromManager(20);
        }
    }

    public void GrappleJoystickUp(Vector2 input)
    {
        lineRenderer.enabled = false;
        targetDot.enabled = false;
        if (hookInstance == null && grappleCharges > 0)
        {
            var direction = new Vector2(-input.x, -input.y).normalized;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

            InstantiateHook(currentHookSpeed, rotation);

            ApplyGrappleCharges(-1);

            AudioManager.instance.PlayFromManager(10);
        }

        // wystrzeliwuje gracza po stop time
        if (joint != null && stopTimeController.isActive)
        {
            player.rig2d.velocity = -joystick.Direction * 200;
            stopTimeController.StopStopTime();
            player.ApplyTorque((Vector2)player.transform.position - joystick.Direction, player.rig2d.velocity.magnitude / 2);
            SeverGrapple();
            var particleEffect = Instantiate(stopTimeParticles, player.transform);
            Destroy(particleEffect, 3);
        }
    }

    public void DisableAiming()
    {
        lineRenderer.enabled = false;
        targetDot.enabled = false;
    }

    public void InstantiateHook(float speed, Quaternion rotation, Vector2? position = null)
    {
        hookInstance = Instantiate(hookPrefab, player.transform.position, rotation);
        hookInstance.grapplingGun = this;
        hookInstance.grappleLimit = grappleLimit;
        hookInstance.speed = speed;

        if (position != null)
        {
            hookInstance.transform.position = position.Value;
        }
    }

    void DrawPredictedLine()
    {
        var direction = new Vector2(-joystick.Horizontal, -joystick.Vertical).normalized;

        var hitInfo = Physics2D.CircleCast(player.transform.position, 1f, direction, grappleLimit.radius, LayerMask.GetMask("InteractableObjects", "Platforms"));

        if (hitInfo)
        {
            lineRenderer.SetPosition(0, player.transform.position);
            lineRenderer.SetPosition(1, hitInfo.point);
            targetDot.transform.position = new Vector3(hitInfo.point.x, hitInfo.point.y, 10);
        }
        else
        {
            var endLinePosition = (Vector2)player.transform.position + (direction * grappleLimit.radius);

            lineRenderer.SetPosition(0, player.transform.position);
            lineRenderer.SetPosition(1, endLinePosition);
            targetDot.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, 10);
        }
    }

    public void SeverGrapple(bool isPlayAudio = false)
    {
        if (hookInstance != null)
        {
            StopGrapple();
            hookInstance.Kill();
            hookInstance = null;
            stopTimeController.StopStopTime();

            EventBroker.CallOnBurnMeterSet(false);
            if (isPlayAudio)
                AudioManager.instance.PlayFromManager(3);
        }
    }

    public void MakeJoint(Vector2 hookPosition, float distance)
    {
        grapplePoint = hookPosition;
        joint = player.gameObject.AddComponent<SpringJoint2D>();

        joint.enableCollision = true;
        joint.autoConfigureConnectedAnchor = false;
        joint.autoConfigureDistance = false;
        joint.distance = 0;
        joint.frequency = currentJointFrequency;
        joint.connectedAnchor = grapplePoint;

        player.ApplyTorque(hookPosition, distance);
        GrappleToCharge();
        ApplyGrappleCharges(1);
        player.ReplenishSlowmo();
    }

    private void GrappleToCharge()
    {
        if (!isGrappleExchangeActive)
            return;

        grappleCount++;
        if (grappleCount >= currentGrappleToChargeExchange)
        {
            if (grappleCharges <= currentMaxGrappleCharges-2)
            {
                grappleCount = 0;
                ApplyGrappleCharges(1);
                EventBroker.CallOnGrappleCountFlash();
                AudioManager.instance.PlayFromManager(13, 0.5f);
            }
            else
            {
                grappleCount--;
            }
        }
        EventBroker.CallOnSetGrappleCount();
    }

    public void ApplyGrappleCharges(int number)
    {
        var newChargesNumber = grappleCharges + number;

        if (newChargesNumber < 0)
        {
            newChargesNumber = 0;
        }

        if (newChargesNumber > currentMaxGrappleCharges)
        {
            newChargesNumber = currentMaxGrappleCharges;
        }

        grappleCharges = newChargesNumber;
        EventBroker.CallOnApplyCharges(grappleCharges);
    }

    public void StopGrapple()
    {
        Destroy(joint);
    }

    /// <summary>
    /// Obsolete. Do wykorzystania kiedy dodam tryb manualnego klikania (bez dżojstika)
    /// </summary>
    void ShootGrappleToPoint()
    {
        if (Input.GetMouseButtonDown(0) && hookInstance == null && grappleCharges > 0)
        {
            Vector3 direction = cam.ScreenToWorldPoint(Input.mousePosition) - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            Quaternion rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);

            hookInstance = Instantiate(hookPrefab, player.transform.position, rotation);
            hookInstance.grapplingGun = this;
            hookInstance.grappleLimit = grappleLimit;

            ApplyGrappleCharges(-1);
        }
    }
}
