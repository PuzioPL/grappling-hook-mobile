using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AboutUIController : MonoBehaviour
{
    public Button yoshButton;

    void Awake()
    {
        yoshButton.onClick.AddListener(() => Application.OpenURL("https://open.spotify.com/artist/4abOqziPBRn4aOQx5zlc6J?si=Rld0t7GlR0-FOKh6lXe5PA&nd=1"));
    }
}
