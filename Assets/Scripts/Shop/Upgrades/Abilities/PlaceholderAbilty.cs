﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Placeholder", menuName = "Upgrade/Placeholder")]
public class PlaceholderAbilty : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        
    }
}
