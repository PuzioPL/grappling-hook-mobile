﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WarningIcon : MonoBehaviour
{
    public Transform target;
    public float smoothTime = 10f;
    private Vector2 velocity = Vector2.zero;

    private void LateUpdate()
    {
        Vector2 desiredPosition = new Vector2(transform.position.x, target.position.y);
        transform.position = Vector2.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothTime);
    }
}
