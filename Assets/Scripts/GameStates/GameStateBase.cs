﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GameStateBase
{
    protected GameStateController stateController;

    protected GameStateBase(GameStateController controller)
    {
        stateController = controller;
    }

    public abstract void OnEnterState();

    public abstract void OnExitState();

    public virtual void StartButtonEvent() { }
    public virtual void PauseButtonEvent() { }
    public virtual void ResumeButtonEvent() { }
    public virtual void BackButtonEvent() { }
    public virtual void DeathEvent() { }
    public virtual void RetryButtonEvent() { }
    public virtual void OptionsButtonEvent() { }
    public virtual void AboutButtonEvent() { }
}
