﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [HideInInspector] public Condition gateCondition;
    public bool isVertical;

    public SpriteRenderer spriteRenderer;
    public bool isOpen;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player") || collision.gameObject.layer == LayerMask.NameToLayer("Grapple"))
        {
            if (gateCondition.Isfulfilled() && !isOpen)
            {
                OpenSesame();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Grapple"))
        {
            var hookScript = collision.gameObject.transform.parent.GetComponent<HookScript>();

            if (hookScript != null)
            {
                hookScript.grapplingGun.SeverGrapple(true);
                if (gateCondition.Isfulfilled() && !isOpen)
                {
                    OpenSesame();
                }
            }
        }
    }

    public void OpenSesame(bool isPlaySound = true)
    {
        isOpen = true;
        spriteRenderer.color = new Color(0, 0.9f, 0);
        if (isVertical)
        {
            LeanTween.moveY(gameObject, gameObject.transform.position.y + 40, 4).setEase(LeanTweenType.easeOutCubic);
        }
        else
        {
            LeanTween.moveX(gameObject, gameObject.transform.position.x + 40, 4).setEase(LeanTweenType.easeOutCubic);
        }

        if (isPlaySound)
            AudioManager.instance.PlayFromManager(21);
    }
}

