using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Starfield : MonoBehaviour
{
    public int maxStars = 100;
    public float starSize = 0.1f;
    public float starSizeRange = 0.5f;
    public float fieldWidth = 20f;
    public float fieldHeight = 25f;
    public bool colorize = false;
    public float parallaxFactor = 0f;
    public Transform camPos;

    float xOffset;
    float yOffset;

    ParticleSystem particles;
    ParticleSystem.Particle[] stars;

    private void Awake()
    {
        stars = new ParticleSystem.Particle[maxStars];
        particles = GetComponent<ParticleSystem>();

        Assert.IsNotNull(particles, "Particle system missing");

        xOffset = fieldWidth * 0.5f;
        yOffset = fieldHeight * 0.5f;

        for (int i = 0; i < maxStars; i++)
        {
            float randSize = Random.Range(starSizeRange, starSizeRange + 1);
            float scaledColor = (colorize) ? randSize - starSizeRange : 1;

            stars[i].position = GetRandomInRectange(fieldWidth, fieldHeight) + (Vector2)transform.position;
            stars[i].startSize = starSize * randSize;
            stars[i].startColor = new Color(1, scaledColor, scaledColor, 1);
        }
        particles.SetParticles(stars, stars.Length);
    }

    private void Update()
    {
        Vector2 newPos = camPos.position * parallaxFactor;
        transform.position = newPos;
    }

    Vector2 GetRandomInRectange(float width, float height)
    {
        float x = Random.Range(0, width);
        float y = Random.Range(0, height);
        return new Vector2(x - xOffset, y - yOffset);
    }
}
