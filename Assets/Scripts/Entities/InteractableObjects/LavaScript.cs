using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaScript : InteractableObjectBase
{
    private void OnEnable()
    {
        IsGrappable = false;
    }

    public override void OnSpawn(ObjectSpawnerController controller) { }

    public override void OnHookCollision(HookScript hook, float distance, Vector2 hookPosition)
    {
        hook.grapplingGun.SeverGrapple(true);
    }

    public override void OnPlayerCollision(PlayerController player)
    {
        EventBroker.CallOnPlayerDeath();
    }
}
