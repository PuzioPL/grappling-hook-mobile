using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftSecretTrigger : MonoBehaviour, ITriggerBehaviour
{
    void ITriggerBehaviour.Act(bool isEnter)
    {
        EventBroker.CallOnPlayerInLeftSecretPresence(isEnter);
    }
}
