﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData
{
    public int currency;
    public UpgradeData[] upgradesData;
    public SkinData[] skinsData;
    public bool isRightItemPicked;
    public bool isLeftItemPicked;
    public bool isTopItemPicked;
    public bool isBetterGraphics;
    public bool isMusicOn;
    public bool isSoundEffectsOn;
    public bool[] gatesData;

    public SaveData(
        int currentCurrency, 
        List<ShopItemBase> shopItems, 
        bool isRightPicked, 
        bool isLeftPicked,
        bool isTopPicked,
        bool isBetterGraphicsValue,
        bool isMusicOnValue,
        bool isSoundEffectsOnValue,
        List<GateHolder> gateHolders)
    {
        currency = currentCurrency;

        var upgradesDataList = new List<UpgradeData>();
        var skinsDataList = new List<SkinData>();

        for (var i = 0; i < shopItems.Count; i++)
        {
            if (shopItems[i] is UpgradeBase)
            {
                var currentUpgrade = shopItems[i] as UpgradeBase;
                var upgradeData = new UpgradeData(currentUpgrade.Id, currentUpgrade.CurrentLevel);

                upgradesDataList.Add(upgradeData);
            }

            if (shopItems[i] is SkinBase)
            {
                var currentSkin = shopItems[i] as SkinBase;
                var skinData = new SkinData(currentSkin.Id, currentSkin.IsUnlocked, currentSkin.IsSelected);

                skinsDataList.Add(skinData);
            }
        }

        // pliki bitowe wymagają użycia arrayów, ale najpierw muszę sprawdzić ile jest upgrade'ów, a ile skinów, żeby móc zdefiniować wielkość arreja
        // dlatego najpierw je wsadzam do listy, a potem zamieniam na array o odpowiednim rozmiarze
        upgradesData = new UpgradeData[upgradesDataList.Count];
        upgradesData = upgradesDataList.ToArray();

        skinsData = new SkinData[skinsDataList.Count];
        skinsData = skinsDataList.ToArray();

        isRightItemPicked = isRightPicked;
        isLeftItemPicked = isLeftPicked;
        isTopItemPicked = isTopPicked;

        isBetterGraphics = isBetterGraphicsValue;

        isMusicOn = isMusicOnValue;
        isSoundEffectsOn = isSoundEffectsOnValue;

        gatesData = new bool[gateHolders.Count];

        for (var i = 0; i < gateHolders.Count; i++)
        {
            gatesData[i] = gateHolders[i].gate.isOpen;
        }
    }
}
