using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphicsButton : MonoBehaviour
{
    [HideInInspector] public Button button;
    public Image icon;
    public Image border;
    public Text text;

    void Awake()
    {
        button = GetComponent<Button>();
    }

    public void SelectButton()
    {
        icon.color = SetOpacity(icon.color, 1);
        border.color = SetOpacity(border.color, 1);
        text.color = SetOpacity(text.color, 1);
    }

    public void DeselectButton()
    {
        icon.color = SetOpacity(icon.color, 0.5f);
        border.color = SetOpacity(border.color, 0.5f);
        text.color = SetOpacity(text.color, 0.5f);
    }

    Color SetOpacity(Color inputColor, float opacity)
    {
        inputColor.a = opacity;
        return inputColor;
    }
}
