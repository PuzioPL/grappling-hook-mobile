﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedSpike : InteractableObjectBase
{
    private void Awake()
    {
        CurrencyReward = 50;
        IsGrappable = false;
        EventBroker.OnRedSpikeUpgrade += () => SetPassable(false);
    }

    public override void OnHookCollision(HookScript hook, float distance, Vector2 hookPosition)
    {
        if (!IsGrappable)
        {
            hook.grapplingGun.SeverGrapple(true);
        }
        else
        {
            EventBroker.CallOnAddCurrency(CurrencyReward, distance, hookPosition);
        }
    }

    public override void OnPlayerCollision(PlayerController player)
    {
        if (!IsGrappable)
        {
            EventBroker.CallOnPlayerDeath();
        }
    }

    public override void Setup(ObjectSpawnerController controller)
    {
        SetPassable(controller.isRedSpikeKilling);
    }

    private void SetPassable(bool isRedKilling)
    {
        IsGrappable = !isRedKilling;
    }
}
