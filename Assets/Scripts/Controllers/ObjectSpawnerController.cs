﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectSpawnerController : MonoBehaviour
{
    public PlayerController player;

    public SpawnZone mainSpawnZone;

    public SpawnZone currentSpawnZoneLeft;
    public SpawnZone currentSpawnZoneRight;

    // localZoneDistance - po to, żeby gęstość spawnowania obiektów była resetowana przy nowej zonie
    public int localZoneDistanceLeft = 0;
    public int localZoneDistanceRight = 0;

    public GameObject parent;

    private float lastSpawnPositionRight = 0;
    private float lastSpawnPositionLeft = 0;

    public float distanceBetweenEdges = 0.25f;
    public float spawnOffset = 400;

    public float maxSpawnHeight = 2000;

    public EnemyController enemy;

    public bool isBlackPassable;
    public bool isRedSpikeKilling;

    // co "distanceFalloffFactor" odległość między spawnowanymi krawędziami jest większa o 1
    public float distanceFalloffFactor = 700; 

    public System.Random rnd;

    private void Awake()
    {
        currentSpawnZoneLeft = mainSpawnZone;
        currentSpawnZoneRight = mainSpawnZone;

        isBlackPassable = false;
        isRedSpikeKilling = true;

        EventBroker.OnBlackEdgeUpgrade += () => isBlackPassable = true;
        EventBroker.OnRedSpikeUpgrade += () => isRedSpikeKilling = false;
    }

    void Start()
    {
        rnd = new System.Random();

        EventBroker.OnPlayerInMazePresence += SetEnemy;
    }

    void Update()
    {
        var playerPosX = player.transform.position.x;

        if (!currentSpawnZoneRight.isDoneSpawning)
        {
            var currentDistanceFalloffRight = (int)((lastSpawnPositionRight - localZoneDistanceRight) / distanceFalloffFactor);

            // spawnowanie po prawej
            if (playerPosX + spawnOffset + distanceBetweenEdges + currentDistanceFalloffRight > lastSpawnPositionRight)
            {
                lastSpawnPositionRight += distanceBetweenEdges + currentDistanceFalloffRight;
                RecalculateWeights(lastSpawnPositionRight, currentSpawnZoneRight);
                SpawnEdge(lastSpawnPositionRight, currentSpawnZoneRight);
            }

            if (Mathf.Abs(lastSpawnPositionRight) > Mathf.Abs(currentSpawnZoneRight.nextZoneThresholdXPos))
            {
                if (currentSpawnZoneRight.nextZoneRight == null)
                {
                    currentSpawnZoneRight.isDoneSpawning = true;
                }
                else
                {
                    localZoneDistanceRight = currentSpawnZoneRight.nextZoneThresholdXPos;
                    currentSpawnZoneRight = currentSpawnZoneRight.nextZoneRight;
                }
            }
        }

        if (!currentSpawnZoneLeft.isDoneSpawning)
        {
            // currentDistanceFalloffLeft - mathematic mumbo-jumbo sprowadza się do tego, że w strefie labiryntu gęstość bloków rośnie, im dalej od początku strefy
            var currentDistanceFalloffLeft = (int)(((lastSpawnPositionLeft * currentSpawnZoneLeft.falloffSign) -  (2*localZoneDistanceLeft)) / distanceFalloffFactor);

            // spawnowanie po lewej
            if (playerPosX - spawnOffset - distanceBetweenEdges - currentDistanceFalloffLeft < lastSpawnPositionLeft)
            {
                lastSpawnPositionLeft -= distanceBetweenEdges - currentDistanceFalloffLeft;
                RecalculateWeights(lastSpawnPositionLeft, currentSpawnZoneLeft);
                SpawnEdge(lastSpawnPositionLeft, currentSpawnZoneLeft);
                //Debug.Log("LastSpawnPositionLeft: " + lastSpawnPositionLeft + ", local zoneDistanceLeft:" + localZoneDistanceLeft + ", result: " + currentDistanceFalloffLeft);
            }

            if (Mathf.Abs(lastSpawnPositionLeft) > Mathf.Abs(currentSpawnZoneLeft.nextZoneThresholdXPos))
            {
                if (currentSpawnZoneLeft.nextZoneLeft == null)
                {
                    currentSpawnZoneLeft.isDoneSpawning = true;
                }
                else
                {

                    localZoneDistanceLeft = currentSpawnZoneLeft.nextZoneThresholdXPos;
                    currentSpawnZoneLeft = currentSpawnZoneLeft.nextZoneLeft;
                }
            }
        }
    }

    void RecalculateWeights(float distance, SpawnZone spawnZone)
    {
        //var forDebug = "New weights at distance " + distance + ": ";

        foreach (var objectAndWeight in spawnZone.objectsInZone)
        {
            var forDebugOld = objectAndWeight.currentWeight;

            float newValue = objectAndWeight.baseWeight + (Mathf.Abs((int)distance) * objectAndWeight.distanceSpawnFalloff);

            objectAndWeight.currentWeight = (int)newValue;

            //forDebug += objectAndWeight.interactableObject + ", "  + forDebugOld  + " " + objectAndWeight.currentWeight;
        }
        //Debug.Log(forDebug);
    }

    void SpawnEdge(float spawnPosX, SpawnZone spawnZone)
    {
        var ypos = Mathf.Floor(Mathf.Abs(UnityEngine.Random.Range(0f, 1f) - UnityEngine.Random.Range(0f, 1f)) * (1 + maxSpawnHeight - (-2)) + (-2));

        if (spawnPosX <= 45 && spawnPosX >= -45 && ypos < 35)
        {
            ypos = Random.Range(20, 2000);
        }

        var posToSpawn = new Vector2(spawnPosX, ypos);

        var objectToSpawn = WeightedRandomizerStatic.From(spawnZone.objectsInZone).TakeOne();

        var spawnedObject = Instantiate(objectToSpawn, posToSpawn, Quaternion.identity, parent.transform);
        spawnedObject.OnSpawn(this);
    }

    void SetEnemy(bool isActivate)
    {
        if (isActivate)
        {
            enemy.gameObject.SetActive(true);

            if (!enemy.isSetup)
                enemy.SetupEnemy();
        }

        enemy.isMovingTowardsPlayer = isActivate;
    }
}

[System.Serializable]
public class InteractableObjectWithWeight
{
    public InteractableObjectBase interactableObject;
    public int baseWeight;
    public int currentWeight;
    public float distanceSpawnFalloff;
}