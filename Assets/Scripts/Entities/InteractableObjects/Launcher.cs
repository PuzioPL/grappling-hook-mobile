﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : InteractableObjectBase
{
    private int launchMinForce = 100;

    void Awake()
    {
        IsGrappable = true;
    }

    public override void OnPlayerCollision(PlayerController player)
    {
        player.grapplingGun.SeverGrapple();

        if (player.rig2d.velocity.magnitude > launchMinForce)
        {
            player.rig2d.velocity = transform.right.normalized * player.rig2d.velocity.magnitude;
        }
        else
        {
            player.rig2d.velocity = transform.right * launchMinForce;
        }

        player.ApplyTorque(transform.position, player.rig2d.velocity.magnitude/2);

        if (player.grapplingGun.grappleCharges == 0)
        {
            player.grapplingGun.ApplyGrappleCharges(1);
        }

        AudioManager.instance.PlayFromManager(5);
    }
    public override void OnHookCollision(HookScript hook, float distance, Vector2 hookPosition)
    {
        var differenceVector = transform.position - hook.grapplingGun.player.transform.position;
        hook.grapplingGun.player.rig2d.velocity = differenceVector.normalized * 100;
    }

    public override void OnSpawn(ObjectSpawnerController controller)
    {
        transform.rotation = Quaternion.Euler(0, 0, Random.Range(45, 135));
    }
}
