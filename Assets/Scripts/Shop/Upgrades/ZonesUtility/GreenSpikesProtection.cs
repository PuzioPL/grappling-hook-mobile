using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GreenSpikesProtection", menuName = "Upgrade/Green Spikes Protection")]
public class GreenSpikesProtection : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        if (CurrentLevel > 0)
            EventBroker.CallOnRedSpikeUpgrade();
    }
}
