﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    bool isTimeAltered;
    float targetTime;

    private bool isPause;

    void Start()
    {
        isTimeAltered = false;

        EventBroker.OnPauseButton += () => isPause = true;
        EventBroker.OnResumeButton += () => isPause = false;
        EventBroker.OnPlayerDeath += () => isPause = false;
    }

    private void Update()
    {
        if (isTimeAltered && Time.timeScale > targetTime)
        {
            var tempTimeScale = Time.timeScale;
            tempTimeScale -= 2 * Time.unscaledDeltaTime;

            if (tempTimeScale < 0)
            {
                tempTimeScale = 0;
            }

            if (tempTimeScale >= 0)
            {
                Time.timeScale = Mathf.Clamp(tempTimeScale, 0, 1);
            }
        }

        if (!isPause && !isTimeAltered && Time.timeScale < 1)
        {
            Time.timeScale += 2 * Time.unscaledDeltaTime;
            Time.timeScale = Mathf.Clamp(Time.timeScale, 0, 1);
        }
    }

    public void DoAlterTime(float slowmoFactor)
    {
        if (!isTimeAltered)
        {
            isTimeAltered = true;
            targetTime = slowmoFactor;
        }
    }

    public void EndAlterTime(bool isEndInstantly)
    {
        if (isTimeAltered)
        {
            isTimeAltered = false;

            if (isEndInstantly)
                Time.timeScale = 1;

            AudioManager.instance.StopLowpassSimple();
        }
    }
}
