﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedEdge : InteractableObjectBase
{
    private void Awake()
    {
        CurrencyReward = 10;
    }

    public override void OnPlayerCollision(PlayerController player)
    {

    }

    public override void OnHookCollision(HookScript hook, float distance, Vector2 hookPosition)
    {
        EventBroker.CallOnAddCurrency(CurrencyReward, distance, hookPosition);

        EventBroker.CallOnBurnMeterSet(true);
    }
}

