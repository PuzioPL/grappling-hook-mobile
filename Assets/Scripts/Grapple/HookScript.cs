﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HookScript : MonoBehaviour
{
    [HideInInspector] public float speed;
    public Rigidbody2D rb;
    public float lifeTime;
    public GrapplingGun grapplingGun;
    private LineRenderer lr;
    public bool isHooked = false;
    public CircleCollider2D grappleLimit;
    public GameObject hookHitEffectPrefab;

    private void Awake()
    {
        lr = GetComponent<LineRenderer>();
    }

    void Start()
    {
        rb.velocity = transform.up * speed;
    }

    public void Kill()
    {
        Destroy(gameObject);
        isHooked = false;
        SpawnHookHitEffect();
    }

    void Update()
    {
        DrawRope();
    }

    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if (!isHooked)
        {
            float distance = Vector3.Distance(grapplingGun.player.transform.position, transform.position);

            if (hitInfo.gameObject.layer == LayerMask.NameToLayer("Platforms"))
            {
                HookToObject(distance);
            }
            else if (hitInfo.gameObject.layer == LayerMask.NameToLayer("InteractableObjects"))
            {
                var interactableObjectScript = hitInfo.GetComponent<InteractableObjectBase>();

                if (interactableObjectScript.IsGrappable)
                {
                    HookToObject(distance);
                }

                if (interactableObjectScript != null)
                {
                    interactableObjectScript.OnHookCollision(this, distance, transform.position);
                }
            }
        }
    }

    private void HookToObject(float distance)
    {
        isHooked = true;
        rb.velocity = Vector2.zero;
        grapplingGun.MakeJoint(transform.position, distance);
        AudioManager.instance.PlayFromManager(2);
        SpawnHookHitEffect();
    }

    private void OnTriggerExit2D(Collider2D hitInfo)
    {
        StartCoroutine(WaitForExit(hitInfo));
    }

    IEnumerator WaitForExit(Collider2D hitInfo)
    {
        // czeka ułamek sekundy na sprawdzenie, żeby OnTriggerEnter i OnTriggerExit nie odpalały się naraz kiedy chwyci na krawędzi
        yield return new WaitForSeconds(0.01f);

        // jak wyjdzie z kolizji z grappleLimit, to usuń hooka (żeby za daleko nie leciał)
        if (!isHooked && hitInfo == grappleLimit)
        {
            Kill();
        }
    }

    void DrawRope()
    {
        lr.SetPosition(0, grapplingGun.player.transform.position);
        lr.SetPosition(1, gameObject.transform.position);
    }

    public void SetSkin(Sprite skinSprite)
    {
        GetComponent<SpriteRenderer>().sprite = skinSprite;
    }

    public void SpawnHookHitEffect()
    {
        var effect = Instantiate(hookHitEffectPrefab, transform.position, transform.rotation);
        Destroy(effect, 1);
    }
}
