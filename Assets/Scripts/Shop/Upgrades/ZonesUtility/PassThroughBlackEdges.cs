﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PassThroughBlackEdges", menuName = "Upgrade/Pass Through Black Edges")]
public class PassThroughBlackEdges : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        if (CurrentLevel > 0)
            EventBroker.CallOnBlackEdgeUpgrade();
    }
}
