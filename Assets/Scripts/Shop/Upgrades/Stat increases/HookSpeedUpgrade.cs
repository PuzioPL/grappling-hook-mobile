﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "HookSpeed", menuName = "Upgrade/Hook Speed")]
public class HookSpeedUpgrade : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        player.grapplingGun.currentHookSpeed = player.grapplingGun.baseHookSpeed + (CurrentLevel * IncreaseValuePerLevel);
    }
}
