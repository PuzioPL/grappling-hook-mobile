using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsUIController : MonoBehaviour
{
    public GraphicsButton betterPerformanceButton;
    public GraphicsButton betterGraphicsButton;
    [HideInInspector] public bool isBetterGraphics;

    void Start()
    {
        betterPerformanceButton.button.onClick.AddListener(() =>
        {
            SetBetterPerformance();
        });

        betterGraphicsButton.button.onClick.AddListener(() =>
        {
            SetBetterGraphics();
        });

        if (isBetterGraphics)
        {
            SetBetterGraphics();
        }
        else
        {
            SetBetterPerformance();
        }
    }

    void SetBetterGraphics()
    {
        SelectButton(betterGraphicsButton, betterPerformanceButton);
        isBetterGraphics = true;
        AudioManager.instance.PlayFromManager(14);
        EventBroker.CallOnGraphicOptions(true);
    }

    void SetBetterPerformance()
    {
        SelectButton(betterPerformanceButton, betterGraphicsButton);
        isBetterGraphics = false;
        AudioManager.instance.PlayFromManager(14);
        EventBroker.CallOnGraphicOptions(false);
    }

    void SelectButton(GraphicsButton buttonToSelect, GraphicsButton buttonToDeselect)
    {
        buttonToSelect.SelectButton();
        buttonToDeselect.DeselectButton();
    }
}
