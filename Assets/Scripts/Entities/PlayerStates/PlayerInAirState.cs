﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInAirState : PlayerBaseState
{
    public override void EnterState(PlayerController player)
    {
    }

    public override void Update(PlayerController player)
    {
    }

    public override void OnCollisionStay(PlayerController player)
    {
        player.TransitionToState(player.GroundState);
    }

    public override void OnCollisionExit(PlayerController player)
    {
    }
}
