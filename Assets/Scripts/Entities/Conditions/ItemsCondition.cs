﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ItemCondition", menuName = "Conditions/Item Condition")]
public class ItemsCondition : Condition
{
    public bool isRightItemRequired;
    public bool isLeftItemRequired;
    private ItemsPickupController itemsPickupController;

    public override void Assign(GatesController gatesController)
    {
        itemsPickupController = gatesController.itemsPickupController;
    }

    public override bool Isfulfilled()
    {
        if (itemsPickupController.rightItem.isPickedUp == isRightItemRequired && itemsPickupController.leftItem.isPickedUp == isLeftItemRequired)
            return true;
        else
            return false;
    }
}
