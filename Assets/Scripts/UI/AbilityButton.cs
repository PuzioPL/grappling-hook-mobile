﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class AbilityButton : MonoBehaviour
{
    public Button button;
    public Text text;
    public Image icon;
    public Image cooldown;
    public Image coverLayer;
    [HideInInspector] public AbilityBase ability;

    public void AssignAbility(AbilityBase abilityUsed, PlayerController player)
    {
        ability = abilityUsed;

        text.text = abilityUsed.Text;
        icon.sprite = abilityUsed.Icon;
        button.gameObject.SetActive(false);
        button.onClick.AddListener(() => {
            ability.Use(player);
            SetOpacity(0.5f);
            });
        ability.IsAlreadyAdded = false;
    }

    public void SetOpacity(float opacity)
    {
        var newColor = coverLayer.color;
        newColor.a = opacity;
        coverLayer.color = newColor;
    }
}
