﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GrappleToCharges", menuName = "Upgrade/Grapple To Charges")]
public class GrappleToChargesExchangeUpgrade : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        if (CurrentLevel > 0)
        {
            player.grapplingGun.isGrappleExchangeActive = true;
        }

        player.grapplingGun.currentGrappleToChargeExchange = player.grapplingGun.baseGrappleToChargeExchange - (CurrentLevel * IncreaseValuePerLevel);
    }
}
