using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AboutState : GameStateBase
{
    public AboutState(GameStateController controller) : base(controller)
    {
    }

    public override void OnEnterState()
    {
        stateController.uiController.SwitchUI(GameStatesEnum.About);
    }

    public override void OnExitState()
    {

    }

    public override void BackButtonEvent()
    {
        stateController.TransitionToState(stateController.MainMenuState);
    }
}
