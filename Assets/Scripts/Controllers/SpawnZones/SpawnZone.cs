﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpawnZone", menuName = "Spawn Zone")]
public class SpawnZone : ScriptableObject
{
    public int nextZoneThresholdXPos;
    public List<InteractableObjectWithWeight> objectsInZone;
    public SpawnZone nextZoneLeft;
    public SpawnZone nextZoneRight;
    [Range(-1f, 1f)] public float falloffSign;
    public bool isDoneSpawning;
    public bool isSpawnEnemy;

    private void OnEnable()
    {
        isDoneSpawning = false;

        foreach(var singleObject in objectsInZone)
        {
            singleObject.currentWeight = singleObject.baseWeight;
        }
    }
}
