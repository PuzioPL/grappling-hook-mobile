using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    public float parallaxValue;

    public float parallaxYOffset;

    private MeshRenderer mesh;

    void Awake()
    {
        mesh = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        Cutout();
        
        UpdateOffset();
    }

    protected virtual void Cutout() { }

    void UpdateOffset()
    {
        var material = mesh.material;
        var offset = material.mainTextureOffset;

        offset.x = transform.transform.position.x / transform.transform.localScale.x / parallaxValue;
        offset.y = (transform.transform.position.y / transform.transform.localScale.y / parallaxValue) + parallaxYOffset;
        material.mainTextureOffset = offset;
    }
}
