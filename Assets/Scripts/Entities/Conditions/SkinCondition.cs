﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SkinCondition", menuName = "Conditions/Skin Condition")]
public class SkinCondition : Condition
{
    public int requiredSkinNumber;
    private SkinSwappingController skinSwappingController;

    public override void Assign(GatesController gatesController)
    {
        skinSwappingController = gatesController.skinSwappingController;
    }

    public override bool Isfulfilled()
    {
        if (skinSwappingController.SelectedPlayerSkin == requiredSkinNumber)
            return true;
        else
            return false;
    }
}
