﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DistanceLimit", menuName = "Upgrade/Distance Limit")]
public class DistanceLimitUpgrade : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        player.grapplingGun.grappleLimit.radius = player.grapplingGun.baseGrappleLimit + (CurrentLevel * IncreaseValuePerLevel);
    }
}
