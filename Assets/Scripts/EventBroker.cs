﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class EventBroker
{
    public static event Action<float> OnGrappleHit;

    public static event Action<int> OnApplyCharges;

    public static event Action OnGrappleSever;

    public static event Action<Vector2> OnJoystickUp;

    public static event Action OnJoystickDown;

    public static event Action<int, float> OnSetUICurrency;

    public static event Action OnSetGrappleCount;

    public static event Action<int, float, Vector2?, Color?> OnAddCurrency;

    public static event Action<float> OnMultiplyCurrency;

    public static event Action<int, Vector2, Color?> OnCurrencyScrollingText;

    public static event Action<bool> OnPlayerInMazePresence;
    public static event Action<bool> OnPlayerInBurnPresence;
    public static event Action<bool> OnPlayerInLeftSecretPresence;

    public static event Action OnBlackEdgeUpgrade;

    public static event Action OnRedSpikeUpgrade;

    public static event Action<ItemSide, bool> OnItemPickup;

    public static event Action<int> OnPlayerSkinChange;
    public static event Action<int> OnHookSkinChange;

    public static event Action OnRespawn;

    public static event Action<bool> OnGraphicOptions;

    public static event Action OnGrappleCountFlash;

    public static event Action<bool> OnSetMusic;
    public static event Action<bool> OnSetSoundEffects;

    public static event Action<bool> OnBurnMeterSet;

    // State changing events
    public static event Action OnStartButton;
    public static event Action OnPauseButton;
    public static event Action OnResumeButton;
    public static event Action OnPlayerDeath;
    public static event Action OnRetryButton;
    public static event Action OnBackArrowButton;
    public static event Action OnOptionsButton;
    public static event Action OnAboutButton;

    public static void CallOnGrappleHit(float distance)
    {
        OnGrappleHit?.Invoke(distance);
    }

    public static void CallOnApplyCharges(int charges)
    {
        OnApplyCharges?.Invoke(charges);
    }

    public static void CallOnGrappleSever()
    {
        OnGrappleSever?.Invoke();
    }

    public static void CallOnJoystickUp(Vector2 input)
    {
        OnJoystickUp?.Invoke(input);
    }

    public static void CallOnJoystickDown()
    {
        OnJoystickDown?.Invoke();
    }

    public static void CallOnSetUICurrency(int currency, float timeToChange = 1)
    {
        OnSetUICurrency?.Invoke(currency, timeToChange);
    }

    public static void CallOnStartButton()
    {
        OnStartButton?.Invoke();
    }

    public static void CallOnPauseButton()
    {
        OnPauseButton?.Invoke();
    }

    public static void CallOnResumeButton()
    {
        OnResumeButton?.Invoke();
    }

    public static void CallOnPlayerDeath()
    {
        OnPlayerDeath?.Invoke();
    }

    public static void CallOnRetryButton()
    {
        OnRetryButton?.Invoke();
    }

    public static void CallOnBackArrowButton()
    {
        OnBackArrowButton?.Invoke();
    }

    public static void CallOnOptionsButton()
    {
        OnOptionsButton?.Invoke();
    }

    public static void CallOnAboutButton()
    {
        OnAboutButton?.Invoke();
    }

    public static void CallOnSetGrappleCount()
    {
        OnSetGrappleCount?.Invoke();
    }

    public static void CallOnAddCurrency(int amount, float distance, Vector2? position, Color? color = null)
    {
        OnAddCurrency?.Invoke(amount, distance, position, color);
    }
    public static void CallOnMultiplyCurrency(float multiplier)
    {
        OnMultiplyCurrency?.Invoke(multiplier);
    }

    public static void CallOnCurrencyScrollingText(int amount, Vector2 position, Color? color = null)
    {
        OnCurrencyScrollingText?.Invoke(amount, position, color);
    }

    public static void CallOnPlayerInMazePresence(bool isEnter)
    {
        OnPlayerInMazePresence?.Invoke(isEnter);
    }

    public static void CallOnPlayerInBurnPresence(bool isEnter)
    {
        OnPlayerInBurnPresence?.Invoke(isEnter);
    }

    public static void CallOnPlayerInLeftSecretPresence(bool isEnter)
    {
        OnPlayerInLeftSecretPresence?.Invoke(isEnter);
    }

    public static void CallOnBlackEdgeUpgrade()
    {
        OnBlackEdgeUpgrade?.Invoke();
    }

    public static void CallOnRedSpikeUpgrade()
    {
        OnRedSpikeUpgrade?.Invoke();
    }

    public static void CallOnItemPickup(ItemSide side, bool showPopup)
    {
        OnItemPickup?.Invoke(side, showPopup);
    }

    public static void CallOnPlayerSkinChange(int skinNumber)
    {
        OnPlayerSkinChange?.Invoke(skinNumber);
    }

    public static void CallOnHookSkinChange(int skinNumber)
    {
        OnHookSkinChange?.Invoke(skinNumber);
    }

    public static void CallOnRespawn()
    {
        OnRespawn?.Invoke();
    }

    public static void CallOnGraphicOptions(bool isChooseBetterGraphics)
    {
        OnGraphicOptions?.Invoke(isChooseBetterGraphics);
    }
    public static void CallOnGrappleCountFlash()
    {
        OnGrappleCountFlash?.Invoke();
    }

    public static void CallOnSetMusic(bool isOn)
    {
        OnSetMusic?.Invoke(isOn);
    }

    public static void CallOnSetSoundEffects(bool isOn)
    {
        OnSetSoundEffects?.Invoke(isOn);
    }

    public static void CallOnBurnMeterSet(bool isStart)
    {
        OnBurnMeterSet?.Invoke(isStart);
    }
}
