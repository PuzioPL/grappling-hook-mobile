﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinButton : ShopItemButtonBase
{
    [Space]
    public GameObject border;

    public override void SetSelected(bool isSelect, Button buyButton = null)
    {
        Color newColor;

        if (isSelect)
        {
            //newColor = new Color(0.2f, 0.6f, 1, currentOpacity);
            newColor = new Color(0.2f, 0.6f, 1);
        }
        else
        {
            //newColor = new Color(0.16f, 0.16f, 0.16f, currentOpacity);
            newColor = new Color(0.16f, 0.16f, 0.16f);
            border.SetActive(false);
        }

        buttonImage.color = newColor;

        if (buyButton != null)
        {
            if (!shopItem.IsMaxed() && !shopItem.IsPlaceholder)
            {
                buyButton.gameObject.SetActive(true);
                var buyButtonImage = buyButton.GetComponent<Image>();
                SetOpacityOnImage(buyButtonImage, mainCurrentOpacity);
            }
            else
            {
                buyButton.gameObject.SetActive(false);
            }
        }
    }

    public override void OnClicked(PlayerController player) 
    { 
        border.SetActive(true);
        shopItem.OnSelect(player);
    }

    public override void DeselectItem()
    {
        if (shopItem.IsSelectedValue())
        {
            shopItem.Deselect();
            border.SetActive(false);
        }
    }

    public override void SetOpaque(int currentCurrency, Button buyButton = null)
    {
        if (!shopItem.IsMaxed() && !shopItem.IsAffordable(currentCurrency))
        {
            coverCurrentOpacity = 0.5f;
            mainCurrentOpacity = 0.5f;
        }
        else
        {
            coverCurrentOpacity = 0f;
            mainCurrentOpacity = 1;
        }

        SetOpacityOnImage(coverLayer, coverCurrentOpacity);

        if (priceText != null)
        {
            var newOpacity = priceText.color;
            newOpacity.a = mainCurrentOpacity;
            priceText.color = newOpacity;
        }

        if (buyButton != null)
        {
            var buyButtonImage = buyButton.GetComponent<Image>();
            SetOpacityOnImage(buyButtonImage, mainCurrentOpacity);
        }
    }

    void SetOpacityOnImage(Image image, float opacity)
    {
        var buyNewColor = image.color;
        buyNewColor.a = opacity;
        image.color = buyNewColor;
    }
}
