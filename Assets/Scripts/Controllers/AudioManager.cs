using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public List<Sound> sounds;
    public List<Sound> musics;

    public static AudioManager instance;
    [SerializeField] private AudioMixer mixer;

    public AudioSource musicSource;

    private AudioMixerGroup soundEffectsGroup;
    private bool isPlayingFanfare = false;

    private bool isMusicOn;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        isMusicOn = true;

        soundEffectsGroup = mixer.FindMatchingGroups("SoundEffects")[0];

        EventBroker.OnSetMusic += (isOn) => TurnMusic(isOn);
        EventBroker.OnSetSoundEffects += (isOn) => TurnSoundEffects(isOn);
    }

    private void Start()
    {
        SetMusicSource(0);
    }

    public void SetMusicSource(int musicId)
    {
        musicSource.clip = musics[musicId].clip;
    }

    public void StopMusic()
    {
        musicSource.Stop();
    }

    public void PlayMusic()
    {
        if (!isPlayingFanfare && isMusicOn)
        {
            musicSource.Play();
            LeanTween.value(this.gameObject, (updatedValue) => mixer.SetFloat("MusicVolume", updatedValue), -80, 0, 0.3f);
        }
    }

    public void TurnMasterVolume(bool isOn)
    {
        mixer.SetFloat("MasterVolume", isOn ? 0 : -80);
    }

    public void TurnMusic(bool isOn)
    {
        isMusicOn = isOn;
        mixer.SetFloat("MusicVolume", isOn ? 0 : -80);
        PlayMusic();
    }

    public void TurnSoundEffects(bool isOn)
    {
        mixer.SetFloat("SoundEffectsVolume", isOn ? 0 : -80);
    }

    public void StartLowpassSimple(float value)
    {
        LeanTween.value(this.gameObject, (updatedValue) => mixer.SetFloat("LowpassSimpleMaster", updatedValue), 22000, value, 0.2f);
    }

    public void StopLowpassSimple()
    {
        LeanTween.cancel(this.gameObject);
        mixer.SetFloat("LowpassSimpleMaster", 22000);
    }

    public void PlayFanfare()
    {
        PlayFromManager(19);
        isPlayingFanfare = true;
    }

    public void StopFanfare() => isPlayingFanfare = false;

    public AudioSource PlayFromManager(int soundNumber, float volumeMultiplier = 1)
    {
        var sound = sounds[soundNumber];

        if (!sound.CanBePlayed())
            return null;

        var audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = sound.clip;
        audioSource.volume = sound.volume * volumeMultiplier;
        audioSource.outputAudioMixerGroup = soundEffectsGroup;

        audioSource.Play();
        sound.lastTimePlayed = Time.time;
        Destroy(audioSource, audioSource.clip.length);
        return audioSource;
    }

    public AudioSource PlayFromObject(int soundNumber, GameObject gObject)
    {
        var sound = sounds[soundNumber];

        if (!sound.CanBePlayed())
            return null;

        var audioSource = gObject.AddComponent<AudioSource>();
        audioSource.clip = sound.clip;
        audioSource.outputAudioMixerGroup = soundEffectsGroup;

        audioSource.maxDistance = 100f;
        audioSource.spatialBlend = 1f;
        audioSource.rolloffMode = AudioRolloffMode.Linear;
        audioSource.dopplerLevel = 0f;
        audioSource.Play();
        sound.lastTimePlayed = Time.time;
        Destroy(audioSource, audioSource.clip.length);
        return audioSource;
    }
}
