﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "QuickGrapple", menuName = "Upgrade/Ability/Quick Grapple")]
public class QuickGrappleAbility : AbilityBase
{
    private List<float> Ranges = new List<float> { 100, 300, 600, 1000 };

    public override void Perform(PlayerController player)
    {
        foreach (var range in Ranges)
        {
            var hitColliders = Physics2D.OverlapCircleAll(player.transform.position, range, LayerMask.GetMask("InteractableObjects"));

            GameObject closestObject = null;
            var closestDistance = range;

            foreach (var collider in hitColliders)
            {
                var distance = Vector2.Distance(player.transform.position, collider.gameObject.transform.position);

                if (collider.gameObject.GetComponent<InteractableObjectBase>().IsGrappable)
                {
                    if (distance < closestDistance)
                    {
                        closestDistance = distance;
                        closestObject = collider.gameObject;
                    }
                }
            }

            if (closestObject != null)
            {
                player.grapplingGun.SeverGrapple();
                player.grapplingGun.InstantiateHook(0, Quaternion.identity, closestObject.transform.position);
                break;
            }
        }
        AudioManager.instance.PlayFromManager(12);
    }

    public override void ApplyItem(PlayerController player)
    {
        if (CurrentLevel > 0)
        {
            IsActive = true;
        }
        else
        {
            IsActive = false;
            IsAlreadyAdded = false;
            NextTimeToUse = 0;
        }

        CurrentCooldown = BaseCooldown - (CurrentLevel * IncreaseValuePerLevel);
        NextTimeToUse = 0;
    }
}
