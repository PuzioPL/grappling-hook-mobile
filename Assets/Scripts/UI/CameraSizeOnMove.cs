using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSizeOnMove : MonoBehaviour
{
    public PlayerController target;
    private Camera cam;

    public float sizeOffset;
    public float baseSize;
    public float sizeFactor; // kamera zmienia przybliżenie, zależnie od prędności gracza
    private float velocitySize = 0f;

    void Start()
    {
        cam = GetComponent<Camera>();
        cam.orthographicSize = baseSize;
    }

    void LateUpdate()
    {
        cam.orthographicSize = Mathf.SmoothDamp(cam.orthographicSize, baseSize + (target.rig2d.velocity.magnitude / sizeFactor) + sizeOffset, ref velocitySize, 0.2f);
    }
}
