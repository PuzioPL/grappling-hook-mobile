using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDirectionsController : MonoBehaviour
{
    public ItemsPickupController itempickupcontroller;

    [SerializeField] private GameObject leftDirection;
    [SerializeField] private GameObject rightDirection;
    [SerializeField] private GameObject topDirection;

    void Awake()
    {
        EventBroker.OnItemPickup += (itemSide, showPopup) => SetDirection(itemSide);
    }

    void Start()
    {
        if (itempickupcontroller.topItem.isPickedUp)
        {
            SetDirection(ItemSide.Top);
        }

        if (itempickupcontroller.leftItem.isPickedUp)
        {
            SetDirection(ItemSide.Left);
        }

        if (itempickupcontroller.rightItem.isPickedUp)
        {
            SetDirection(ItemSide.Right);
        }
    }

    void SetDirection(ItemSide itemSide)
    {
        if (itemSide == ItemSide.Right)
        {
            rightDirection.SetActive(false);
        }

        if (itemSide == ItemSide.Left)
        {
            leftDirection.SetActive(false);
        }

        if (itemSide == ItemSide.Top)
        {
            topDirection.SetActive(false);
        }
    }
}
