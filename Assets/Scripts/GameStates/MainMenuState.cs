﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuState : GameStateBase
{
    public MainMenuState(GameStateController controller) : base(controller)
    {
    }

    public override void OnEnterState()
    {
        stateController.uiController.SwitchUI(GameStatesEnum.MainMenu);
        stateController.uiController.SetJoystick(false);
    }

    public override void OnExitState()
    {
    }

    public override void StartButtonEvent()
    {
        stateController.TransitionToState(stateController.PlayState);
    }

    public override void DeathEvent()
    {
        stateController.TransitionToState(stateController.ShopState);
    }

    public override void OptionsButtonEvent()
    {
        stateController.TransitionToState(stateController.OptionsState);
    }

    public override void AboutButtonEvent()
    {
        stateController.TransitionToState(stateController.AboutState);
    }
}
