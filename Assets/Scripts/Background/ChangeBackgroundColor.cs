using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBackgroundColor : MonoBehaviour
{
    public Color baseColor;
    public Color mazeColor;
    public Color burnColor;

    private MeshRenderer mesh;

    void Start()
    {
        mesh = GetComponent<MeshRenderer>();

        EventBroker.OnPlayerInMazePresence += (isEnter) =>
        {
            if (isEnter)
            {
                LeanTween.value(gameObject, UpdateColor, baseColor, mazeColor, 1);
            }
            else
            {
                LeanTween.value(gameObject, UpdateColor, mazeColor, baseColor, 1);
            }
        };

        EventBroker.OnPlayerInBurnPresence += (isEnter) =>
        {
            if (isEnter)
            {
                LeanTween.value(gameObject, UpdateColor, baseColor, burnColor, 1);
            }
            else
            {
                LeanTween.value(gameObject, UpdateColor, burnColor, baseColor, 1);
            }
        };
    }

    protected void UpdateColor(Color colorValue)
    {
        mesh.material.color = colorValue;
    }
}
