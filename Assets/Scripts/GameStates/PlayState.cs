﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayState : GameStateBase
{
    public PlayState(GameStateController controller) : base(controller)
    {
    }

    public override void OnEnterState()
    {
        stateController.uiController.SwitchUI(GameStatesEnum.Play);
        stateController.uiController.SetJoystick(true);
    }

    public override void OnExitState()
    {
    }

    public override void DeathEvent()
    {
        stateController.uiController.DisableUI();
        LeanTween.delayedCall(1.5f, () => stateController.TransitionToState(stateController.ShopState));
    }

    public override void PauseButtonEvent()
    {
        stateController.TransitionToState(stateController.PauseState);
    }
}
