﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewSkin", menuName = "Skin")]
public class SkinBase : ShopItemBase
{
    [Header("Skin specific properties")]
    public int Price;
    public bool IsUnlocked;
    public bool IsSelected;
    public int SkinNumber;

    public override bool IsSelectedValue()
    {
        return IsSelected;
    }

    public override void Buy(PlayerController player)
    {
        IsSelected = true;
        IsUnlocked = true;
        ApplyItem(player);
    }

    public override string GetButtonMainText()
    {
        if (!IsUnlocked && !IsPlaceholder)
        {
            return $"{Text}\n{Price}$";
        }
        else
        {
            return $"{Text}\n";
        }
    }

    public override string GetButtonPriceText()
    {
        return "";
    }

    public override int GetPrice()
    {
        return Price;
    }

    public override bool IsAffordable(int currentCurrency)
    {
        var tempCurrency = currentCurrency - Price;

        if (tempCurrency >= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public override bool IsMaxed()
    {
        return IsUnlocked;
    }

    public override void OnSelect(PlayerController player)
    {
        if (IsUnlocked)
        {
            IsSelected = true;
            ApplyItem(player);
        }
    }

    public override void ApplyItem(PlayerController player)
    {
        if (!IsSelected)
            return;

        if (Type == ShopItemTypeEnum.SkinPlayer)
        {
            EventBroker.CallOnPlayerSkinChange(SkinNumber);
            //player.SetSkin(Icon);
        }

        if (Type == ShopItemTypeEnum.SkinHook)
        {
            EventBroker.CallOnHookSkinChange(SkinNumber);
            //player.grapplingGun.hookPrefab.SetSkin(Icon);
        }
    }

    public override void ReadSaveData(SaveableDataBase data = null)
    {
        if (data != null)
        {
            var skinData = data as SkinData;
            IsUnlocked = skinData.isUnlocked;
            IsSelected = skinData.isSelected;
        }
        else
        {
            IsUnlocked = false;
            IsSelected = false;
        }
    }

    public override void Deselect()
    {
        IsSelected = false;
    }
}
