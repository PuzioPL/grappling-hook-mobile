using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class MuteAudioButtonBase : MonoBehaviour
{
    public Sprite onSprite;
    public Sprite offSprite;
    //[SerializeField] private Image image;
    [SerializeField] private Image image;
    [SerializeField] private Button button;

    [HideInInspector] public bool isAudioOn = true;

    private void Awake()
    {
        button.onClick.AddListener(() => SwitchAudio());
    }

    private void OnEnable()
    {
        //SetSprite();
    }

    void SetSprite()
    {
        if (isAudioOn)
        {
            image.sprite = onSprite;
        }
        else
        {
            image.sprite = offSprite;
        }
    }

    public void SetAudio(bool isOn)
    {
        isAudioOn = isOn;
        SetSprite();
        SendEvent();
    }

    public void SwitchAudio()
    {
        isAudioOn = !isAudioOn;
        SetSprite();
        SendEvent();
    }

    public abstract void SendEvent();
}
