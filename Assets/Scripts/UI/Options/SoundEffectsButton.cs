using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectsButton : MuteAudioButtonBase
{
    public override void SendEvent()
    {
        EventBroker.CallOnSetSoundEffects(isAudioOn);
    }
}
