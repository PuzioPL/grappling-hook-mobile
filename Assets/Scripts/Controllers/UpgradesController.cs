﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class UpgradesController : MonoBehaviour
{
    public PlayerController player;

    public List<UpgradeBase> UpgradesList;

    public AbilityBase ability1;
    public AbilityBase ability2;

    public void Awake()
    {
        UpgradesList.Add(ability1);
        UpgradesList.Add(ability2);
    }

    public void SetUpgradesLevel(UpgradeData[] upgradesData = null)
    {
        /*
        if (upgradesData != null)
        {
            foreach(var upgrade in UpgradesList)
            {
                var chosenUpgrade = upgradesData.Where(p => p.id == upgrade.Id).FirstOrDefault();

                if (chosenUpgrade != null)
                {
                    upgrade.CurrentLevel = chosenUpgrade.currentLevel;
                }
                else
                {
                    break;
                }
                upgrade.ApplyUpgrade(player);
            }
        }
        else
        {
            foreach (var upgrade in UpgradesList)
            {
                upgrade.CurrentLevel = 0;
                upgrade.ApplyUpgrade(player);
            }
        }
        */
    }
}
