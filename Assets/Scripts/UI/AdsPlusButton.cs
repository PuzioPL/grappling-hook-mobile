using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdsPlusButton : MonoBehaviour
{
    private Button button;
    private RectTransform rect;
    private bool isPulsing = false;

    private void Awake()
    {
        button = GetComponent<Button>();
        rect = GetComponent<RectTransform>();
    }

    void OnEnable()
    {
        SetPulsing(true);
    }

    public void Initialize(Action action)
    {
        button.onClick.AddListener(() =>
        {
            action?.Invoke();
            SetPulsing(false);
            AudioManager.instance.PlayFromManager(0);
        });
    }

    public void SetPulsing(bool pulse)
    {
        if (pulse)
        {
            if (!isPulsing)
            {
                isPulsing = true;
                LeanTween.size(rect, new Vector2(25, 25), 1).setLoopPingPong();
            }
        }
        else
        {
            isPulsing = false;
            LeanTween.cancel(rect);
            rect.sizeDelta = new Vector2(15, 15);
        }
    }    
}
