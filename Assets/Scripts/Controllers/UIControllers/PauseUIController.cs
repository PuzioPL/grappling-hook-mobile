﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseUIController : MonoBehaviour
{
    public Button resumeButton;
    public Button dieButton;

    void Start()
    {
        resumeButton.onClick.AddListener(() =>
        {
            AudioManager.instance.PlayFromManager(1);
            EventBroker.CallOnResumeButton();
        });

        dieButton.onClick.AddListener(() =>
        {
            EventBroker.CallOnPlayerDeath();
        });
    }
}
