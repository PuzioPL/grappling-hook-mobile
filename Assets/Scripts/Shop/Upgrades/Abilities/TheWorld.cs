﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TheWorld", menuName = "Upgrade/The World")]
public class TheWorld : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        if (CurrentLevel > 0)
        {
            player.grapplingGun.stopTimeController.isActive = true;
        }
    }
}
