using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class CameraPostProcessingSwitch : MonoBehaviour
{
    private UniversalAdditionalCameraData UAC_cam;

    void Awake()
    {
        UAC_cam = GetComponent<UniversalAdditionalCameraData>();
        EventBroker.OnGraphicOptions += SetPostProcessing;
    }

    void SetPostProcessing(bool isOn)
    {
        UAC_cam.renderPostProcessing = isOn;
    }
}
