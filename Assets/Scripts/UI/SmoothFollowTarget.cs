﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollowTarget : MonoBehaviour
{
    public PlayerController target;
    public float smoothTime = 10f;
    private Vector3 velocityPos = Vector3.zero;
    public Vector3 offset;

    public Vector2 positionOffset;
    public float velocityVectorFactor; // kamera "wyprzedza" gracza, zależnie od jego prędności

    private void Start()
    {
        EventBroker.OnRespawn += () => transform.position = new Vector3(0, 0, -1);
    }

    private void LateUpdate()
    {
        var playerVelocity = target.rig2d.velocity;

        Vector3 desiredPosition = (Vector2)target.transform.position + (playerVelocity / velocityVectorFactor) + positionOffset;

        transform.position = Vector3.SmoothDamp(transform.position, desiredPosition + offset, ref velocityPos, smoothTime);
    }
}
