﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemPickup : InteractableObjectBase
{
    public LeanTweenType loopEaseType;
    public LeanTweenType dissapearEaseType;
    [HideInInspector] public string Info;
    public Vector2 startPosition;
    public float movementTime;
    public ItemSide side;
    public int currencyReward; 

    public void OnEnable()
    {
        IsGrappable = false;
        gameObject.transform.localPosition = startPosition;
        LeanTween.moveY(gameObject, transform.position.y - 10, movementTime).setLoopPingPong().setEase(loopEaseType);
    }

    public override void OnHookCollision(HookScript hook, float distance, Vector2 hookPosition)
    {
        PickupGrabbed(hook.grapplingGun);
    }

    public override void OnPlayerCollision(PlayerController player)
    {
        PickupGrabbed(player.grapplingGun);
    }

    void PickupGrabbed(GrapplingGun grapplingGun)
    {
        grapplingGun.SeverGrapple();
        PickupGrabbedAnimation();
        AudioManager.instance.PlayFromManager(18);
        EventBroker.CallOnItemPickup(side, true);
        EventBroker.CallOnAddCurrency(currencyReward, 0, transform.position);
    }

    void PickupGrabbedAnimation()
    {
        LeanTween.cancel(gameObject);
        LeanTween.alpha(gameObject, 0, 0.5f);
        LeanTween.moveY(gameObject, transform.position.y + 7, 0.5f).setEase(dissapearEaseType).setDestroyOnComplete(true);
    }
}
