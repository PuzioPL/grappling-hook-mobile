﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private PlayerController player;
    private Rigidbody2D rig2d;
    private Transform spriteTransform;
    private Vector2 movementDirection;
    public float baseSpeed;
    public float currentSpeed;
    public float acceleration;
    public bool isMovingTowardsPlayer;
    public bool isSetup;

    public float maxTwitchDistance;
    public float minTimeBetweenTwitches;
    public float maxTimeBetweenTwitches;
    public float nextTwitchTime;
    private AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        isSetup = false;
        isMovingTowardsPlayer = false;
        currentSpeed = baseSpeed;
        rig2d = GetComponent<Rigidbody2D>();
        audioSource = GetComponent<AudioSource>();

        player = FindObjectOfType<PlayerController>();
        spriteTransform = transform.GetChild(0).transform;
        EventBroker.OnPlayerDeath += () => currentSpeed = baseSpeed;
        EventBroker.OnRespawn += () => transform.position = new Vector2(-4000, 1000);
    }

    private void Update()
    {
        if (isMovingTowardsPlayer)
        {
            movementDirection = player.gameObject.transform.position - this.transform.position;
            currentSpeed += acceleration * Time.deltaTime;
        }

        if (Time.time >= nextTwitchTime)
        {
            Twitch();
        }
    }

    void Twitch()
    {
        float xTwitchingDistance;
        float yTwitchingDistance;

        // szansa, że zamiast odchylenia w inną stronę, wróci do swojej pierwotnej pozycji
        if (Random.Range(0f, 1f) > 0.2f)
        {
            xTwitchingDistance = Random.Range(-maxTwitchDistance, maxTwitchDistance);
            yTwitchingDistance = Random.Range(-maxTwitchDistance, maxTwitchDistance);
        }
        else
        {
            xTwitchingDistance = 0;
            yTwitchingDistance = 0;
        }

        spriteTransform.position = new Vector2(transform.position.x + xTwitchingDistance, transform.position.y + yTwitchingDistance);

        nextTwitchTime = Time.time + Random.Range(minTimeBetweenTwitches, maxTimeBetweenTwitches);
    }

    void FixedUpdate()
    {
        if (isMovingTowardsPlayer)
        {
            rig2d.AddForce(movementDirection.normalized * currentSpeed, ForceMode2D.Force);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            audioSource.volume = 0.05f;
            isSetup = false;
            EventBroker.CallOnPlayerDeath();
        }
    }

    public void SetupEnemy()
    {
        isSetup = true;
        Vector2 setupPosition = player.transform.position;
        var randomValue = Random.insideUnitCircle.normalized;

        // -Mathf.Abs(), żeby przeciwnik zawsze się ustawiał po lewej stronie gracza
        setupPosition += new Vector2(-Mathf.Abs(randomValue.x), randomValue.y) * 1000;

        transform.position = setupPosition;
        audioSource.volume = 1;
    }
}
