﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseState : GameStateBase
{
    public PauseState(GameStateController controller) : base(controller)
    {
    }

    public override void OnEnterState()
    {
        stateController.uiController.SwitchUI(GameStatesEnum.Pause);
        stateController.uiController.SetJoystick(false);
        Time.timeScale = 0;
    }

    public override void OnExitState()
    {
        Time.timeScale = 1;
    }

    public override void ResumeButtonEvent()
    {
        stateController.TransitionToState(stateController.PlayState);
    }

    public override void DeathEvent()
    {
        stateController.uiController.DisableUI();
        stateController.uiController.blackUIBackground.SetActive(false);
        LeanTween.delayedCall(1.5f, () => stateController.TransitionToState(stateController.ShopState));
    }
}
