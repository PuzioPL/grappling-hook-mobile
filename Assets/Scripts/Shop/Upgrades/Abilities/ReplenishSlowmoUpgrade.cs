using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ReplenishSlowmo", menuName = "Upgrade/Replenish Slowmo")]
public class ReplenishSlowmoUpgrade : UpgradeBase
{
    public override void ApplyItem(PlayerController player)
    {
        player.currentReplenishSlowmoValue = player.baseReplenishSlowmoValue + (CurrentLevel * IncreaseValuePerLevel);
    }
}
