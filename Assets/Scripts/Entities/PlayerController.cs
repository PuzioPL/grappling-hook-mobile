﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D rig2d;

    public CurrencyData_SO currencyData;

    private PlayerBaseState currentState;

    public GrapplingGun grapplingGun;
    public float refreshChargeInterval;
    [HideInInspector] public float nextTimeToRefreshCharge;
    public SkinSwappingController skinSwappingController;

    public float baseReplenishSlowmoValue;
    public float currentReplenishSlowmoValue;
    public BurnCanvas burnMeterCanvas;

    public PlayerBaseState CurrentState
    {
        get { return currentState; }
    }

    public readonly PlayerOnGroundState GroundState = new PlayerOnGroundState();
    public readonly PlayerInAirState AirState = new PlayerInAirState();

    public float speed;

    private void Awake()
    {
        rig2d = GetComponent<Rigidbody2D>();

        EventBroker.OnRespawn += Respawn;

        TransitionToState(AirState);
        EventBroker.OnPlayerDeath += SoftDie;
        EventBroker.OnBurnMeterSet += (isStart) =>
        {
            if (isStart)
            {
                burnMeterCanvas.gameObject.SetActive(true);
                burnMeterCanvas.StartBurn();
            }
            else
            {
                burnMeterCanvas.StopBurn();
            }

        };
    }

    private void Update()
    {
        currentState.Update(this);
    }

    private void FixedUpdate()
    {
        speed = (int)rig2d.velocity.magnitude;
    }

    public void RefreshCharges()
    {
        if (Time.time < nextTimeToRefreshCharge)
            return;

        nextTimeToRefreshCharge = Time.time + refreshChargeInterval;
        grapplingGun.ApplyGrappleCharges(1);
    }
    
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.layer == LayerMask.NameToLayer("InteractableObjects"))
        {
            var interactableObject = collider.gameObject.GetComponent<InteractableObjectBase>();

            if (interactableObject != null)
            {
                interactableObject.OnPlayerCollision(this);
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.gameObject.layer == LayerMask.NameToLayer("Platforms"))
        {
            currentState.OnCollisionStay(this);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Platforms"))
        {
            currentState.OnCollisionExit(this);
        }
    }

    public void TransitionToState(PlayerBaseState state)
    {
        currentState = state;
        currentState.EnterState(this);
    }

    public void ApplyTorque(Vector2 position, float mult)
    {
        var diff = position - (Vector2)transform.position;

        var torqueSign = -1;

        if (diff.x <= 0)
        {
            // grappling hook zachaczony po lewej stronie -> torque dodatni
            torqueSign = 1;
        }

        rig2d.AddTorque(torqueSign * (mult * 2), ForceMode2D.Impulse);
    }

    public void ReplenishSlowmo()
    {
        var newSlowmoDurationLeft = grapplingGun.slowmoController.slowmoDurationLeft + currentReplenishSlowmoValue;
        grapplingGun.slowmoController.slowmoDurationLeft = Mathf.Min(newSlowmoDurationLeft, grapplingGun.slowmoController.currentMaxSlowmoDuration);
        grapplingGun.slowmoController.SetSlowmoBarManually();
    }

    public void SoftDie()
    {
        grapplingGun.joystick.OnPointerUp(null);

        grapplingGun.DisableAiming();

        grapplingGun.SeverGrapple(false);
        rig2d.velocity = Vector2.zero;
        rig2d.bodyType = RigidbodyType2D.Static;
        grapplingGun.grappleCharges = 0;
        //gameObject.GetComponent<SpriteRenderer>().enabled = false;
        skinSwappingController.DisablePlayerSkinSprite();
        skinSwappingController.PlayDeathParticles();
        AudioManager.instance.PlayFromManager(6);
        CameraShakeScript.instance.CamShake();
    }

    public void Respawn()
    {
        grapplingGun.slowmoController.slowmoDurationLeft = grapplingGun.slowmoController.currentMaxSlowmoDuration;
        gameObject.transform.position = new Vector3(0, 7, 0);
        gameObject.transform.rotation = Quaternion.identity;
        //tr.enabled = true;
        grapplingGun.ApplyGrappleCharges(grapplingGun.currentMaxGrappleCharges);
        grapplingGun.grappleCount = 0;
        rig2d.bodyType = RigidbodyType2D.Dynamic;
        //gameObject.GetComponent<SpriteRenderer>().enabled = true;
        skinSwappingController.EnablePlayerSkinSprite();
        skinSwappingController.StopDeathParticles();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.magnitude > 2)
        {
            AudioManager.instance.PlayFromManager(7, Mathf.Min(collision.relativeVelocity.magnitude / 12, 1));
        }
    }
}
