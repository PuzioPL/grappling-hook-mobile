# Grappling the Sun

[Google Play Link.](https://play.google.com/store/apps/details?id=com.BowlCutCat.GrapplingtheSun)

[YouTube presentation link.](https://www.youtube.com/watch?v=ajdRGkfdcAc) 

**Description**

My first fully released game! Available for Android on Google Play Store for free!

Grappling the Sun is a 2D arcade game, where player uses grappling hook to move around. The player can also buy upgrades from the shop, using earned currency from grabbing objects, which will help him stay in air for longer periods of time and move farther from the starting point. In the shop the player can also buy skins that change the look of his character and the grappling hook (purely cosmetic).
There is also a special room at the top of an area, that unlocks when the player acquires two key items, located at the opposide edges of an area. It serves as a sort of end game goal.

Game created using Unity, URP and C#. The code is a HUGE upgrade from my „Project-polygon” prototype – I used a lot more prefabs, scriptable objects, abstracts and state machines. 

Project created mostly for myself – I wanted to make a whole game from start to finish for quite some time, and even though it is not perfect (especially some parts of the code!) I am still happy with the results. 

I won’t be updating this game, maybe excluding some bugfixes - I already have few other ideas for games that I want to develop.

**Features**
1. Ability to grapple objects and move around by breaking the grappling line at the correct time. Shooting and breaking the grapple adapted for touch controls.

2. Three distinct zones, each with their own object types and special mechanics:
- starting zone (blue) – contains launcher and booster objects, that can allow for fast traveling if used correctly. Gold edges provide huge bonus currency, whereas red spikes can kill the player on contact. Green edges are only for grappling and do not have any special interactions.

<img src="Assets/Screenshots/1.png" alt="Starting zone" width="720">

- maze zone (green) – contains mostly gold and blue edges. Blue edges can be grappled on, but the player character cannot move through them. It makes moving through this zone slow on purpose, because it also contains an enemy – an object that slowly moves toward the player’s position. Enemy’s speed gets faster each second the player is present in a zone, so if the player cannot move fast enough, he will be eventually killed.

<img src="Assets/Screenshots/2.png" alt="Green zone" width="720">

- burn zone (red) – contains red edges, red spikes and golden edges. Red edges constantly „burn” the player when grabbed, shown by the burn meter above the character. If the burn meter is filled, the player is immediately killed.

<img src="Assets/Screenshots/3.png" alt="Red zone" width="720">

3. Random object placement in each zone, with a few rules:
- the higher from the start point, the fewer objects are present.
- the farther in X axis from the start point, the less or more chance for different objects to appear. For example red spikes are more likely to appear away from the start point.

4. The in-game shop that contains: 
-  15 different upgrades that significantly change how the player interacts with the world.
- 6 character skins, some of which have a special particle effects.
- 6 grappling hook skins.

<img src="Assets/Screenshots/4.PNG" alt="Shop" width="720">

5. Two special key items, hidden behind different challenges.

6. One special zone, unlockable with previously mentioned keys. The zone contains a pickup, that provide the player with bonus currency and unlocks exclusive skins.

<img src="Assets/Screenshots/5.png" alt="Top zone" width="720">

7. Two tracks, created by my friend Yoshua! [Check him out!](https://open.spotify.com/artist/4abOqziPBRn4aOQx5zlc6J?si=Rld0t7GlR0-FOKh6lXe5PA&nd=1)
