﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateController : MonoBehaviour
{
    public UIController uiController;

    private GameStateBase currentState;

    public GameStateBase CurrentState
    {
        get { return currentState; }
    }

    public MainMenuState MainMenuState;
    public PlayState PlayState;
    public PauseState PauseState;
    public ShopState ShopState;
    public OptionsState OptionsState;
    public AboutState AboutState;

    private void Awake()
    {
        MainMenuState = new MainMenuState(this);
        PlayState = new PlayState(this);
        ShopState = new ShopState(this);
        PauseState = new PauseState(this);
        OptionsState = new OptionsState(this);
        AboutState = new AboutState(this);

        EventBroker.OnStartButton += () => currentState.StartButtonEvent();
        EventBroker.OnPlayerDeath += () => currentState.DeathEvent();
        EventBroker.OnRetryButton += () => currentState.RetryButtonEvent();
        EventBroker.OnPauseButton += () => currentState.PauseButtonEvent();
        EventBroker.OnResumeButton += () => currentState.ResumeButtonEvent();
        EventBroker.OnBackArrowButton += () => currentState.BackButtonEvent();
        EventBroker.OnOptionsButton += () => currentState.OptionsButtonEvent();
        EventBroker.OnAboutButton += () => currentState.AboutButtonEvent();
    }

    private void Start()
    {
        TransitionToState(MainMenuState);
    }

    public void TransitionToState(GameStateBase state)
    {
        if (currentState != null)
            currentState.OnExitState();

        currentState = state;
        currentState.OnEnterState();
    }
}

public enum GameStatesEnum { MainMenu, Play, Pause, Shop, Options, About }
