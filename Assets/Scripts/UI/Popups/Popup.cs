﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Popup : MonoBehaviour
{
    public Image icon;
    public TextMeshProUGUI text;
    private float timeShown;

    public void Initialize(Sprite popupIconSprite, string popupText, float timeShownValue)
    {
        icon.sprite = popupIconSprite;
        text.text = popupText;
        timeShown = timeShownValue;
    }

    void Start()
    {
        Destroy(gameObject, timeShown);
    }
}
