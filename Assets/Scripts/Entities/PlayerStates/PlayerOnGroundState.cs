﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerOnGroundState : PlayerBaseState
{
    public override void EnterState(PlayerController player)
    {
        player.nextTimeToRefreshCharge = Time.time + player.refreshChargeInterval;
    }

    public override void Update(PlayerController player)
    {
        player.RefreshCharges();
    }

    public override void OnCollisionStay(PlayerController player)
    {
    }

    public override void OnCollisionExit(PlayerController player)
    {
        player.TransitionToState(player.AirState);
    }
}
