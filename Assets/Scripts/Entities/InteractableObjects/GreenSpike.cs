﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenSpike : InteractableObjectBase
{
    void Awake()
    {
        IsGrappable = false;
    }

    public override void OnHookCollision(HookScript hook, float distance, Vector2 hookPosition)
    {
        StartCoroutine(Wait(hook));
    }

    public override void OnPlayerCollision(PlayerController player)
    {
        EventBroker.CallOnPlayerDeath();
    }

    // dirty fix to a bug
    IEnumerator Wait(HookScript hook)
    {
        yield return new WaitForSeconds(0.01f);

        hook.grapplingGun.SeverGrapple(true);
    }
}
