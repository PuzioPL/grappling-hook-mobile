﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScrollingText : MonoBehaviour
{
    private TextMeshPro textMesh;
    private float timeToFadeOut;

    private void Awake()
    {
        textMesh = transform.GetComponent<TextMeshPro>();
    }

    public void Setup(string text, Color? color)
    {
        textMesh.SetText(text);
        timeToFadeOut = Time.time + 0.5f;

        if (color != null)
        {
            textMesh.color = color.Value;
        }

        Destroy(gameObject, 3f);
    }

    private void Update()
    {
        transform.position += new Vector3(0, 10, 0) * Time.deltaTime;

        if (Time.time >= timeToFadeOut)
        {
            var tempColor = textMesh.color;
            tempColor.a -= 2 * Time.deltaTime;
            textMesh.color = tempColor;
        }
    }
}
