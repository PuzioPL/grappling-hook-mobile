﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopTrigger : MonoBehaviour, ITriggerBehaviour
{
    void ITriggerBehaviour.Act(bool isEnter)
    {
        if (isEnter)
        {
            AudioManager.instance.SetMusicSource(1);
            AudioManager.instance.PlayMusic();
        }
        else
        {
            // zmień muzykę spowrotem na "vert1cal"
            AudioManager.instance.SetMusicSource(0);
            AudioManager.instance.PlayMusic();
        }
    }
}